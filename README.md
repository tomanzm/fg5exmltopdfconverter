## Fantasy Grounds Character XML to PDF Converter

This is a relatively simple tool to convert D&D 5e Fantasy Grounds Character XML files to usable pdfs.

I've included the the executable version of this application in the Converter.zip file, included [here](https://bitbucket.org/tomanzm/fg5exmltopdfconverter/downloads/XmlPdfConverter.zip) (v1.0 4/23).
This requires the pdfs in this file: [srcPdfs.zip](https://bitbucket.org/tomanzm/fg5exmltopdfconverter/downloads/srcPdfs.zip) (v1.0 4/23)

Those two packages should have everything you need besides the actual character xml file.

The extracted structure should look something like:

    Converter
        LaunchConverter.bat
        XmlPdfConverter.jar
        srcPdfs
            pdf files

**This requires JAVA 1.8+**

**It turns out Windows Machines are not fans JAVA Executables, you can launch the application with the included LaunchConverter.bat (It does take a hot second to start)**

Otherwise feel free to download the repo and do with it what you will

##Fiddly Bits
1. This application only uses what is avalible in the Fantasy Grounds 5e Character XML files, and if they change the structure drastically, this will break.
2. Prepared spells are added with priority given to spells you have selected as prepared, until there are no more spots to add spells
3. Description pages are appended to the end of the pdf, spell descriptions are not added, but all of the other ones that are available are...that I can think of.
4. This assumes that your backstory is stored in your notes tab.
5. Height details and the other small details are not separately stored in the Character xml file, thus they are not auto filled.
6. Some fields in the pdfs have hard limits on their lengths, I've tried to do everything I can to mitigate issues around them, but there's only so much I can do.
7. Injecting images into the image spots are not supported at this point via the application.
8. I may have made questionable decisions in some areas, I'm always open to potential alternatives.
9. To associate spells with a class, this checks for the class name in the power group name, or for the class that can cast, but doesn't have class name in the power group name. For example if you are just a warlock, the standard "Spells" power group will be associated with it, but if you are a warlock/bard and have a "Spells" and "Spells (Bard)" powergroup, the warlock with be associated with the "Spells" groupe and the bard with the "Spells (Bard)" group

##Notes:
1. If you have custom character sheets you would like to use, so long as it is for 5e and has fillable fields, feel free to put in a request for conversion (or do it yourself ;) a file of interest should be the Converter.java)
2. I plan to add some flags to add more customization to what details you want displayed about your character, or how you want them displayed (I am aware how some of you madlads like your ability scores and bonuses flipped ;) ).
3. I may integrate other Fantasy Grounds Character Sheet Systems if there's demand
4. This may be way down the line, but I do also want to implement a structure I can use to convert between the Fantasy Grounds Character xml files and Generic Pdfs...This might not even be feasible, but hey, goals.