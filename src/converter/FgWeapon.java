package converter;

public class FgWeapon {
	private int attackBonus;
	private String attackStat;
	private String[][] damage;/* [[bonus,dice,stat,statMult,type],[bonus,dice,stat,statMult,type]] */
	private String name;
	private boolean proficient;
	private String properties;

	public FgWeapon() {
	}

	public int getAttackBonus() {
		return attackBonus;
	}

	public void setAttackBonus(int attackBonus) {
		this.attackBonus = attackBonus;
	}

	public String getAttackStat() {
		return attackStat;
	}

	public void setAttackStat(String attackStat) {
		this.attackStat = attackStat;
	}

	public String[][] getDamage() {
		return damage;
	}

	public void setDamage(String[][] damage) {
		this.damage = damage;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isProficient() {
		return proficient;
	}

	public void setProficient(boolean proficient) {
		this.proficient = proficient;
	}

	public String getProperties() {
		return properties;
	}

	public void setProperties(String properties) {
		this.properties = properties;
	}

}
