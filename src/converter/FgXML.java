package converter;

import java.io.ByteArrayInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class FgXML {
	private String threadName;

	private String characterName;
	private HashMap<String, FgAbility> abilities;/* Stat, ability details */
	private ArrayList<FgAdventure> adventures;
	private String age;
	private String alignment;
	private String appearance;
	private String background;
	private String bonds;
	private FgClass[] classes;
	private HashMap<String, Integer> coinage;
	private String coinOther;
	private String dci;
	private int ac;
	private String specialDefenses;
	private String diety;
	private float encumbered;
	private float encumberedHeavy;
	private float liftPushDrag;
	private float encumbrance;
	private float encumberedMax;
	private int exp;
	private int expNeeded;
	private String faction;
	private HashMap<String, String> feats;
	private HashMap<String, ArrayList<String[]>> features;/* Source, [name,description] */
	private String flaws;
	private String gender;
	private String height;
	private int temporaryHp;
	private int totalHp;
	private int wound;
	private String ideals;
	private int init;
	private int inspiration;
	private FgInventoryItem[] inventory;
	private String[] languages;
	private int totalLevel;
	private String notes;
	private int passivePerc;
	private String personalityTraits;
	private HashMap<String, FgClassPowers> powerGroups; /* group, associatedSpells */
	private int[] pactSlots;
	private int[] spellSlots;
	private int profBonus;
	private String[] proficiencies;
	private String race;
	private String senses;
	private HashMap<String, int[]> skills; /* name , [proficient?, total] */
	private int speed;
	private String specialSpeed;
	private String[][] traits; /* [[name, description],[name, description]] */
	private FgWeapon[] weapons;
	private String weight;

	public FgXML(String path) {
		threadName = "Thread " + Thread.currentThread().getId() + ": ";
		try {
			/* get the file and remove the space characters from between the tags */
			String fileString = new String(Files.readAllBytes(Paths.get(path))).replaceAll(">\\s+?<", "><");

			/* convert it to a usable format */
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			dbFactory.setIgnoringComments(true);
			DocumentBuilder dBuilder;
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(new ByteArrayInputStream(fileString.getBytes()));

			/* get rid of file data as it's no longer necessary */
			fileString = "";

			/* fill out the above variables */
			buildStructure(doc);

		} catch (Exception e) {
			System.out.println(threadName + "Error: " + e);
			e.printStackTrace(System.out);
			e.printStackTrace();
		}
	}

	private void buildStructure(Document doc) {
		doc.getDocumentElement().normalize();
		Element root = (Element) doc.getFirstChild().getFirstChild();
		NodeList elements = root.getChildNodes();

		for (int i = 0; i < elements.getLength(); ++i) {
			Node node = elements.item(i);
			Element element = (Element) node;
			// System.out.println(node.getNodeName());
			String nodeValue = "";
			if (node.getChildNodes().getLength() == 1)
				nodeValue = node.getTextContent();

			switch (node.getNodeName().toLowerCase()) {
			case "abilities":
				abilities = new HashMap<String, FgAbility>();
				abilities.put("Strength", setAbility("strength", element));
				abilities.put("Dexterity", setAbility("dexterity", element));
				abilities.put("Constitution", setAbility("constitution", element));
				abilities.put("Intelligence", setAbility("intelligence", element));
				abilities.put("Wisdom", setAbility("wisdom", element));
				abilities.put("Charisma", setAbility("charisma", element));
				abilities.put("Unset", setAbility("charisma", element));
				break;
			case "adventurelist":
				NodeList advs = node.getChildNodes();
				for (int j = 0; j < advs.getLength(); ++j) {
					Element adv = (Element) advs.item(j);
					FgAdventure adventure = new FgAdventure();
					adventure.setAdvDate(adv.getElementsByTagName("date").getLength() > 0
							? adv.getElementsByTagName("date").item(0).getTextContent()
							: "");
					adventure.setGmName(adv.getElementsByTagName("dmname").getLength() > 0
							? adv.getElementsByTagName("dmname").item(0).getTextContent()
							: "");
					adventure.setAdventureName(adv.getElementsByTagName("name").getLength() > 0
							? adv.getElementsByTagName("name").item(0).getTextContent()
							: "");
					adventure.setSessionNum(adv.getElementsByTagName("sessionno").getLength() > 0
							? adv.getElementsByTagName("sessionno").item(0).getTextContent()
							: "");
					adventure.setStartingXP(adv.getElementsByTagName("startxp").getLength() > 0
							? adv.getElementsByTagName("startxp").item(0).getTextContent()
							: "");
					adventure.setEarnedXP(adv.getElementsByTagName("earnxp").getLength() > 0
							? adv.getElementsByTagName("earnxp").item(0).getTextContent()
							: "");
					adventure.setTotalXP(adv.getElementsByTagName("totalxp").getLength() > 0
							? adv.getElementsByTagName("totalxp").item(0).getTextContent()
							: "");
					adventure.setStartingGold(adv.getElementsByTagName("startgold").getLength() > 0
							? adv.getElementsByTagName("startgold").item(0).getTextContent()
							: "");
					adventure.setEarnedGold(adv.getElementsByTagName("earngold").getLength() > 0
							? adv.getElementsByTagName("earngold").item(0).getTextContent()
							: "");
					adventure.setTotalGold(adv.getElementsByTagName("totalgold").getLength() > 0
							? adv.getElementsByTagName("totalgold").item(0).getTextContent()
							: "");
					adventure.setStartingDT(adv.getElementsByTagName("startdt").getLength() > 0
							? adv.getElementsByTagName("startdt").item(0).getTextContent()
							: "");
					adventure.setEarnedDT(adv.getElementsByTagName("earndt").getLength() > 0
							? adv.getElementsByTagName("earndt").item(0).getTextContent()
							: "");
					adventure.setTotalDT(adv.getElementsByTagName("totaldt").getLength() > 0
							? adv.getElementsByTagName("totaldt").item(0).getTextContent()
							: "");
					adventure.setStartingRN(adv.getElementsByTagName("startrenown").getLength() > 0
							? adv.getElementsByTagName("startrenown").item(0).getTextContent()
							: "");
					adventure.setEarnedRN(adv.getElementsByTagName("earnrenown").getLength() > 0
							? adv.getElementsByTagName("earnrenown").item(0).getTextContent()
							: "");
					adventure.setTotalRN(adv.getElementsByTagName("totalrenown").getLength() > 0
							? adv.getElementsByTagName("totalrenown").item(0).getTextContent()
							: "");
					adventure.setAdvDtNotes(adv.getElementsByTagName("text").getLength() > 0
							? structuredText(adv.getElementsByTagName("text").item(0))
							: "");
					adventure.setStartingMgc(adv.getElementsByTagName("startmagic").getLength() > 0
							? adv.getElementsByTagName("startmagic").item(0).getTextContent()
							: "");
					adventure.setEarnedMgc(adv.getElementsByTagName("earnmagic").getLength() > 0
							? adv.getElementsByTagName("earnmagic").item(0).getTextContent()
							: "");
					adventure.setTotalMgc(adv.getElementsByTagName("totalmagic").getLength() > 0
							? adv.getElementsByTagName("totalmagic").item(0).getTextContent()
							: "");
					adventure.setStartingT2Mgc(adv.getElementsByTagName("treasure_pt_tier2").getLength() > 0
							? adv.getElementsByTagName("treasure_pt_tier2").item(0).getTextContent()
							: "");
					adventure.setEarnedT2Mgc(adv.getElementsByTagName("earn_treasure_pt_tier2").getLength() > 0
							? adv.getElementsByTagName("earn_treasure_pt_tier2").item(0).getTextContent()
							: "");
					adventure.setTotalT2Mgc(adv.getElementsByTagName("total_treasure_pt_tier2").getLength() > 0
							? adv.getElementsByTagName("total_treasure_pt_tier2").item(0).getTextContent()
							: "");
					adventure.setStartingT3Mgc(adv.getElementsByTagName("treasure_pt_tier3").getLength() > 0
							? adv.getElementsByTagName("treasure_pt_tier3").item(0).getTextContent()
							: "");
					adventure.setEarnedT3Mgc(adv.getElementsByTagName("earn_treasure_pt_tier3").getLength() > 0
							? adv.getElementsByTagName("earn_treasure_pt_tier3").item(0).getTextContent()
							: "");
					adventure.setTotaltT3Mgc(adv.getElementsByTagName("total_treasure_pt_tier3").getLength() > 0
							? adv.getElementsByTagName("total_treasure_pt_tier3").item(0).getTextContent()
							: "");
					adventure.setStartingT4Mgc(adv.getElementsByTagName("treasure_pt_tier4").getLength() > 0
							? adv.getElementsByTagName("treasure_pt_tier4").item(0).getTextContent()
							: "");
					adventure.setEarnedT4Mgc(adv.getElementsByTagName("earn_treasure_pt_tier4").getLength() > 0
							? adv.getElementsByTagName("earn_treasure_pt_tier4").item(0).getTextContent()
							: "");
					adventure.setTotaltT4Mgc(adv.getElementsByTagName("total_treasure_pt_tier4").getLength() > 0
							? adv.getElementsByTagName("total_treasure_pt_tier4").item(0).getTextContent()
							: "");
					adventure.setMgcItmUnlocked(adv.getElementsByTagName("magicitem").getLength() > 0
							? structuredText(adv.getElementsByTagName("magicitem").item(0))
							: "");
					adventure.setStryRwds(adv.getElementsByTagName("storyrewards").getLength() > 0
							? structuredText(adv.getElementsByTagName("storyrewards").item(0))
							: "");
					adventure.setPurchases(adv.getElementsByTagName("purchases").getLength() > 0
							? structuredText(adv.getElementsByTagName("purchases").item(0))
							: "");
				}
				break;
			case "age":
				age = nodeValue;
				break;
			case "alignment":
				alignment = nodeValue;
				break;
			case "appearance":
				appearance = handleLayouts(nodeValue);
				break;
			case "bonds":
				bonds = handleLayouts(nodeValue);
				break;
			case "background":
				background = nodeValue;
				break;
			case "classes":
				classes = new FgClass[node.getChildNodes().getLength()];
				NodeList fgClasses = node.getChildNodes();
				for (int j = 0; j < fgClasses.getLength(); ++j) {
					Element fgClass = (Element) fgClasses.item(j);
					FgClass meta = new FgClass();
					meta.setCaster(fgClass.getElementsByTagName("casterlevelinvmult").getLength() > 0 && Integer
							.parseInt(fgClass.getElementsByTagName("casterlevelinvmult").item(0).getTextContent()) > 0);
					meta.setPactCaster(fgClass.getElementsByTagName("casterpactmagic").getLength() > 0 && Integer
							.parseInt(fgClass.getElementsByTagName("casterpactmagic").item(0).getTextContent()) > 0);
					meta.setHitDie(fgClass.getElementsByTagName("hddie").item(0).getTextContent());
					meta.setLevel(Integer.parseInt(fgClass.getElementsByTagName("level").item(0).getTextContent()));
					meta.setName(fgClass.getElementsByTagName("name").item(0).getTextContent());
					if (meta.isCaster() && root.getElementsByTagName("powergroup").getLength() > 0) {
						Node tnode = null;
						NodeList pGroups = ((Element) root.getElementsByTagName("powergroup").item(0)).getChildNodes();
						for (int k = 0; k < pGroups.getLength(); ++k) {
							Node aNode = pGroups.item(k);
							if (((Element) aNode).getElementsByTagName("name").item(0).getTextContent()
									.equals("Spells"))
								tnode = aNode;
							else if (((Element) aNode).getElementsByTagName("name").item(0).getTextContent()
									.contains(meta.getName())) {
								tnode = aNode;
								break;
							}
						}
						meta.setSpellGroupName(((Element) tnode).getElementsByTagName("name").item(0).getTextContent());
						meta.setSpellStat(((Element) tnode).getElementsByTagName("stat").getLength() > 0
								? ((Element) tnode).getElementsByTagName("stat").item(0).getTextContent()
								: "unset");
					}
					classes[j] = meta;
				}
				break;
			case "coins":
				coinage = new HashMap<String, Integer>();
				NodeList coins = node.getChildNodes();
				for (int j = 0; j < coins.getLength(); ++j) {
					if (((Element) coins.item(j)).getElementsByTagName("name").getLength() > 0) {
						String cName = ((Element) coins.item(j)).getElementsByTagName("name").item(0).getTextContent();
						if (!cName.trim().isEmpty())
							coinage.put(cName, Integer.parseInt(
									((Element) coins.item(j)).getElementsByTagName("amount").item(0).getTextContent()));
					}
				}
				break;
			case "coinother":
				coinOther = handleLayouts(nodeValue);
				break;
			case "dci":
				dci = nodeValue;
				break;
			case "defenses":
				ac = Integer.parseInt(((Element) element.getElementsByTagName("ac").item(0))
						.getElementsByTagName("total").item(0).getTextContent());
				if (element.getElementsByTagName("special").getLength() > 0)
					specialDefenses = element.getElementsByTagName("special").item(0).getTextContent();
				break;
			case "deity":
				diety = nodeValue;
				break;
			case "encumbrance":
				encumbered = Float.parseFloat(element.getElementsByTagName("encumbered").item(0).getTextContent());
				encumberedHeavy = Float
						.parseFloat(element.getElementsByTagName("encumberedheavy").item(0).getTextContent());
				liftPushDrag = Float.parseFloat(element.getElementsByTagName("liftpushdrag").item(0).getTextContent());
				encumbrance = Float.parseFloat(element.getElementsByTagName("load").item(0).getTextContent());
				encumberedMax = Float.parseFloat(element.getElementsByTagName("max").item(0).getTextContent());
				break;
			case "exp":
				exp = Integer.parseInt(nodeValue);
				break;
			case "expneeded":
				expNeeded = Integer.parseInt(nodeValue);
				break;
			case "faction":
				faction = nodeValue;
				break;
			case "featlist":
				feats = new HashMap<String, String>();
				NodeList fgFeats = node.getChildNodes();
				for (int j = 0; j < fgFeats.getLength(); ++j) {
					Element fgFeat = (Element) fgFeats.item(j);
					feats.put(fgFeat.getElementsByTagName("name").item(0).getTextContent(),
							structuredText(fgFeat.getElementsByTagName("text").item(0)));
				}
				break;
			case "featurelist":
				features = new HashMap<String, ArrayList<String[]>>();
				NodeList fgFeatures = node.getChildNodes();
				for (int j = 0; j < fgFeatures.getLength(); ++j) {
					Element fgFeature = (Element) fgFeatures.item(j);
					String key;
					if (fgFeature.getElementsByTagName("source").getLength() > 0)
						key = fgFeature.getElementsByTagName("source").item(0).getTextContent();
					else
						key = "Unsourced";
					if (!features.containsKey(key))
						features.put(key, new ArrayList<String[]>());
					String[] value = { fgFeature.getElementsByTagName("name").item(0).getTextContent(),
							structuredText(fgFeature.getElementsByTagName("text").item(0)) };
					features.get(key).add(value);
				}
				break;
			case "flaws":
				flaws = handleLayouts(nodeValue);
				break;
			case "gender":
				gender = nodeValue;
				break;
			case "height":
				height = nodeValue;
				break;
			case "hp":
				temporaryHp = Integer.parseInt(element.getElementsByTagName("temporary").item(0).getTextContent());
				totalHp = Integer.parseInt(element.getElementsByTagName("total").item(0).getTextContent());
				wound = Integer.parseInt(element.getElementsByTagName("wounds").item(0).getTextContent());
				break;
			case "ideals":
				ideals = handleLayouts(nodeValue);
				break;
			case "initiative":
				init = Integer.parseInt(element.getElementsByTagName("total").item(0).getTextContent());
				break;
			case "inspiration":
				inspiration = Integer.parseInt(nodeValue);
				break;
			case "inventorylist":
				NodeList invNodes = element.getChildNodes();
				inventory = new FgInventoryItem[invNodes.getLength()];
				for (int j = 0; j < invNodes.getLength(); ++j) {
					boolean identified = true;
					Element invNode = (Element) invNodes.item(j);
					FgInventoryItem item = new FgInventoryItem();
					NodeList temp = invNode.getElementsByTagName("carried");
					item.setCarriedStatus(Integer.parseInt(temp.item(0).getTextContent()));
					temp = invNode.getElementsByTagName("cost");
					if (temp.getLength() > 0)
						item.setCost(temp.item(0).getTextContent());
					temp = invNode.getElementsByTagName("count");
					if (temp.getLength() > 0)
						item.setCount(Integer.parseInt(temp.item(0).getTextContent()));
					temp = invNode.getElementsByTagName("isidentified");
					if (temp.getLength() > 0) {
						identified = Integer.parseInt(temp.item(0).getTextContent()) > 0;
						item.setIdentified(identified);
					}
					temp = invNode.getElementsByTagName("damage");
					if (temp.getLength() > 0)
						item.setDamage(temp.item(0).getTextContent());
					temp = invNode.getElementsByTagName("description");
					item.setDescription(temp.getLength() > 0 ? structuredText(temp.item(0)) : "");
					temp = invNode.getElementsByTagName("location");
					if (temp.getLength() > 0)
						item.setLocation(temp.item(0).getTextContent());
					temp = invNode.getElementsByTagName("name");
					item.setName(temp.getLength() > 0 ? temp.item(0).getTextContent() : "<Name Missing>");
					temp = invNode.getElementsByTagName("nonid_name");
					if (temp.getLength() > 0)
						item.setNonIdName(temp.item(0).getTextContent());
					temp = invNode.getElementsByTagName("nonidentified");
					if (temp.getLength() > 0)
						item.setNonIdDescription(handleLayouts(temp.item(0).getTextContent()));
					temp = invNode.getElementsByTagName("rarity");
					if (temp.getLength() > 0)
						item.setRarity(temp.item(0).getTextContent());
					temp = invNode.getElementsByTagName("properties");
					if (temp.getLength() > 0)
						item.setProperties(temp.item(0).getTextContent());
					temp = invNode.getElementsByTagName("subtype");
					if (temp.getLength() > 0)
						item.setSubtype(temp.item(0).getTextContent());
					temp = invNode.getElementsByTagName("type");
					if (temp.getLength() > 0)
						item.setType(temp.item(0).getTextContent());
					temp = invNode.getElementsByTagName("weight");
					item.setWeight(Float.parseFloat(temp.item(0).getTextContent()));
					inventory[j] = item;
				}
				Arrays.sort(inventory);
				break;
			case "languagelist":
				NodeList langList = element.getChildNodes();
				languages = new String[langList.getLength()];
				for (int j = 0; j < langList.getLength(); ++j)
					languages[j] = langList.item(j).getTextContent();
				break;
			case "level":
				totalLevel = Integer.parseInt(nodeValue);
				break;
			case "name":
				characterName = nodeValue;
				break;
			case "notes":
				notes = handleLayouts(nodeValue);
				break;
			case "perception":
				passivePerc = Integer.parseInt(nodeValue);
				break;
			case "personalitytraits":
				personalityTraits = handleLayouts(nodeValue);
				break;
			case "powermeta":
				NodeList slots = element.getChildNodes();
				int[] temP = { Integer.parseInt("0" + slots.item(0).getTextContent()),
						Integer.parseInt("0" + slots.item(1).getTextContent()),
						Integer.parseInt("0" + slots.item(2).getTextContent()),
						Integer.parseInt("0" + slots.item(3).getTextContent()),
						Integer.parseInt("0" + slots.item(4).getTextContent()),
						Integer.parseInt("0" + slots.item(5).getTextContent()),
						Integer.parseInt("0" + slots.item(6).getTextContent()),
						Integer.parseInt("0" + slots.item(7).getTextContent()),
						Integer.parseInt("0" + slots.item(8).getTextContent()), };
				int[] temS = { Integer.parseInt("0" + slots.item(9).getTextContent()),
						Integer.parseInt("0" + slots.item(10).getTextContent()),
						Integer.parseInt("0" + slots.item(11).getTextContent()),
						Integer.parseInt("0" + slots.item(12).getTextContent()),
						Integer.parseInt("0" + slots.item(13).getTextContent()),
						Integer.parseInt("0" + slots.item(14).getTextContent()),
						Integer.parseInt("0" + slots.item(15).getTextContent()),
						Integer.parseInt("0" + slots.item(16).getTextContent()),
						Integer.parseInt("0" + slots.item(17).getTextContent()), };
				pactSlots = temP;
				spellSlots = temS;
				break;
			case "powers":
				NodeList fgPowers = element.getChildNodes();
				powerGroups = new HashMap<String, FgClassPowers>();
				for (int j = 0; j < fgPowers.getLength(); ++j) {
					Element fgPowerElm = (Element) fgPowers.item(j);
					FgPower power = new FgPower();
					if (fgPowerElm.getElementsByTagName("name").getLength() > 0) {
						String group = (fgPowerElm.getElementsByTagName("group").getLength() > 0
								? fgPowerElm.getElementsByTagName("group").item(0).getTextContent()
								: "Ungrouped");
						power.setDescription(fgPowerElm.getElementsByTagName("description").getLength() > 0
								? structuredText(fgPowerElm.getElementsByTagName("description").item(0))
								: "");
						power.setName(fgPowerElm.getElementsByTagName("name").item(0).getTextContent());
						power.setPrepared(fgPowerElm.getElementsByTagName("prepared").getLength() > 0
								? Integer.parseInt(
										fgPowerElm.getElementsByTagName("prepared").item(0).getTextContent()) > 0
								: false);
						if (group.toLowerCase().contains("spell")) {
							power.setComponents(fgPowerElm.getElementsByTagName("components").getLength() > 0
									? fgPowerElm.getElementsByTagName("components").item(0).getTextContent()
									: "");
							power.setDuration(fgPowerElm.getElementsByTagName("duration").getLength() > 0
									? fgPowerElm.getElementsByTagName("duration").item(0).getTextContent()
									: "");
							power.setRange(fgPowerElm.getElementsByTagName("range").getLength() > 0
									? fgPowerElm.getElementsByTagName("range").item(0).getTextContent()
									: "");
							if (!powerGroups.containsKey(group))
								powerGroups.put(group, new FgClassPowers(true));
							switch (Integer
									.parseInt(fgPowerElm.getElementsByTagName("level").item(0).getTextContent())) {
							case 0:
								powerGroups.get(group).getCantrips().add(power);
								break;
							case 1:
								powerGroups.get(group).getLevel1().add(power);
								break;
							case 2:
								powerGroups.get(group).getLevel2().add(power);
								break;
							case 3:
								powerGroups.get(group).getLevel3().add(power);
								break;
							case 4:
								powerGroups.get(group).getLevel4().add(power);
								break;
							case 5:
								powerGroups.get(group).getLevel5().add(power);
								break;
							case 6:
								powerGroups.get(group).getLevel6().add(power);
								break;
							case 7:
								powerGroups.get(group).getLevel7().add(power);
								break;
							case 8:
								powerGroups.get(group).getLevel8().add(power);
								break;
							case 9:
								powerGroups.get(group).getLevel9().add(power);
								break;
							}
						} else {
							if (!powerGroups.containsKey(group))
								powerGroups.put(group, new FgClassPowers(false));
							powerGroups.get(group).getCantrips().add(power);
						}
					}
				}

				break;
			case "profbonus":
				profBonus = Integer.parseInt(nodeValue);
				break;
			case "proficiencylist":
				NodeList profList = element.getChildNodes();
				proficiencies = new String[profList.getLength()];
				for (int j = 0; j < profList.getLength(); ++j)
					proficiencies[j] = profList.item(j).getTextContent();
				break;
			case "race":
				race = nodeValue;
				break;
			case "senses":
				senses = nodeValue;
				break;
			case "skilllist":
				skills = new HashMap<String, int[]>();
				NodeList fgSkills = element.getChildNodes();
				for (int j = 0; j < fgSkills.getLength(); ++j) {
					Element fgSkill = (Element) fgSkills.item(j);
					int[] temp = {
							(fgSkill.getElementsByTagName("prof").getLength() > 0
									? Integer.parseInt(fgSkill.getElementsByTagName("prof").item(0).getTextContent())
									: 0),
							Integer.parseInt(fgSkill.getElementsByTagName("total").item(0).getTextContent()) };
					skills.put(fgSkill.getElementsByTagName("name").item(0).getTextContent(), temp);
				}
				break;
			case "speed":
				speed = Integer.parseInt(element.getElementsByTagName("total").item(0).getTextContent());
				if (element.getElementsByTagName("special").getLength() > 0)
					specialSpeed = element.getElementsByTagName("special").item(0).getTextContent();
				break;
			case "traitlist":
				NodeList fgTraits = element.getChildNodes();
				traits = new String[fgTraits.getLength()][2];
				for (int j = 0; j < fgTraits.getLength(); ++j) {
					Element fgTrait = (Element) fgTraits.item(j);
					traits[j][0] = fgTrait.getElementsByTagName("name").item(0).getTextContent();
					traits[j][1] = structuredText(fgTrait.getElementsByTagName("text").item(0));
				}
				break;
			case "weaponlist":
				NodeList fgWeapons = element.getChildNodes();
				weapons = new FgWeapon[fgWeapons.getLength()];
				for (int j = 0; j < fgWeapons.getLength(); ++j) {
					Element fgWep = (Element) fgWeapons.item(j);
					FgWeapon weapon = new FgWeapon();
					weapon.setName(fgWep.getElementsByTagName("name").getLength() > 0
							? fgWep.getElementsByTagName("name").item(0).getTextContent()
							: "");
					weapon.setAttackBonus(fgWep.getElementsByTagName("attackbonus").getLength() > 0
							? Integer.parseInt(fgWep.getElementsByTagName("attackbonus").item(0).getTextContent())
							: 0);
					weapon.setAttackStat(fgWep.getElementsByTagName("attackstat").getLength() > 0
							? fgWep.getElementsByTagName("attackstat").item(0).getTextContent()
							: "base");
					NodeList dmgs = fgWep.getElementsByTagName("damagelist");
					String[][] damageList = new String[dmgs.getLength()][5];
					for (int k = 0; k < dmgs.getLength(); ++k) {
						Element dmg = (Element) dmgs.item(k);
						damageList[k][0] = (dmg.getElementsByTagName("bonus").getLength() > 0
								? dmg.getElementsByTagName("bonus").item(0).getTextContent()
								: "0");
						damageList[k][1] = (dmg.getElementsByTagName("dice").getLength() > 0
								? dmg.getElementsByTagName("dice").item(0).getTextContent()
								: "");
						damageList[k][2] = (dmg.getElementsByTagName("stat").getLength() > 0
								? dmg.getElementsByTagName("stat").item(0).getTextContent()
								: "");
						damageList[k][3] = (dmg.getElementsByTagName("statmult").getLength() > 0
								? dmg.getElementsByTagName("statmult").item(0).getTextContent()
								: "1");
						damageList[k][4] = (dmg.getElementsByTagName("type").getLength() > 0
								? dmg.getElementsByTagName("type").item(0).getTextContent()
								: "");
					}
					weapon.setDamage(damageList);
					weapon.setName(fgWep.getElementsByTagName("name").getLength() > 0
							? fgWep.getElementsByTagName("name").item(0).getTextContent()
							: "");
					weapon.setProficient(
							Integer.parseInt(fgWep.getElementsByTagName("prof").item(0).getTextContent()) > 0);
					weapon.setProperties(fgWep.getElementsByTagName("properties").getLength() > 0
							? fgWep.getElementsByTagName("properties").item(0).getTextContent()
							: "");
					weapons[j] = weapon;
				}
				break;
			case "weight":
				weight = nodeValue;
				break;
			}
		}
		System.out.println(threadName + "XML parsing complete");
	}

	private static FgAbility setAbility(String tag, Element element) {
		FgAbility temFocus = new FgAbility();
		if (!tag.equals("Unset")) {
			Element temp = (Element) (element.getElementsByTagName(tag).item(0));
			temFocus.setBonus(Integer.parseInt(temp.getElementsByTagName("bonus").item(0).getTextContent()));
			temFocus.setSave(Integer.parseInt(((Element) temp).getElementsByTagName("save").item(0).getTextContent()));
			temFocus.setSaveProf(Integer.parseInt(temp.getElementsByTagName("saveprof").item(0).getTextContent()) > 0);
			temFocus.setScore(Integer.parseInt(temp.getElementsByTagName("score").item(0).getTextContent()));
		} else {
			temFocus.setBonus(0);
			temFocus.setSave(0);
			temFocus.setSaveProf(false);
			temFocus.setScore(0);
		}
		// System.out.println(temFocus.toString());
		return temFocus;
	}

	private static String structuredText(Node text) {
		String out = "";
		NodeList nl = text.getChildNodes();
		for (int i = 0; i < nl.getLength(); ++i) {
			Node tempNode = nl.item(i);
			if (!tempNode.getTextContent().trim().isEmpty())
				switch (tempNode.getNodeName().toLowerCase()) {
				case "p":
				case "h":
					out += tempNode.getTextContent().trim() + "\n";
					break;
				case "frame":
					out += "|Quote|" + tempNode.getTextContent().trim() + "|Quote|\n";
					break;
				case "list":
					NodeList fgListElms = tempNode.getChildNodes();
					for (int j = 0; j < fgListElms.getLength(); ++j)
						out += "-" + fgListElms.item(j).getTextContent().trim() + "\n";
					out += "\n";
					break;
				case "table":
					NodeList fgRows = tempNode.getChildNodes();
					for (int j = 0; j < fgRows.getLength(); ++j) {
						NodeList fgCells = fgRows.item(j).getChildNodes();
						out += "|";
						for (int k = 0; k < fgCells.getLength(); ++k)
							out += fgCells.item(k).getTextContent().trim() + "|";
						out += "\n";
					}
					out += "\n";
					break;
				case "linklist":
//					if (false) /* make a variable to control this */
//						out += "#" + tempNode.getTextContent().trim() + "\n";
					break;
				default:
					out += tempNode.getTextContent().trim();
				}
			out += "\n";
		}
		return out;
	}

	private static String handleLayouts(String in) {
		/* (?<!\\)\\n only grab it if it's not preceded by a slash */
		in = in.replaceAll("(?<!\\\\)\\\\n", "\n");
		return in;
	}

	public String getCharacterName() {
		return characterName;
	}

	public HashMap<String, FgAbility> getAbilities() {
		return abilities;
	}

	public ArrayList<FgAdventure> getAdventures() {
		return adventures;
	}

	public String getAge() {
		return age;
	}

	public String getAlignment() {
		return alignment;
	}

	public String getAppearance() {
		return appearance;
	}

	public String getBackground() {
		return background;
	}

	public String getBonds() {
		return bonds;
	}

	public FgClass[] getClasses() {
		return classes;
	}

	public HashMap<String, Integer> getCoinage() {
		return coinage;
	}

	public String getCoinOther() {
		return coinOther;
	}

	public String getDci() {
		return dci;
	}

	public int getAc() {
		return ac;
	}

	public String getSpecialDefenses() {
		return specialDefenses;
	}

	public String getDiety() {
		return diety;
	}

	public float getEncumbered() {
		return encumbered;
	}

	public float getEncumberedHeavy() {
		return encumberedHeavy;
	}

	public float getLiftPushDrag() {
		return liftPushDrag;
	}

	public float getEncumbrance() {
		return encumbrance;
	}

	public float getEncumberedMax() {
		return encumberedMax;
	}

	public int getExp() {
		return exp;
	}

	public int getExpNeeded() {
		return expNeeded;
	}

	public String getFaction() {
		return faction;
	}

	public HashMap<String, String> getFeats() {
		return feats;
	}

	public HashMap<String, ArrayList<String[]>> getFeatures() {
		return features;
	}

	public String getFlaws() {
		return flaws;
	}

	public String getGender() {
		return gender;
	}

	public String getHeight() {
		return height;
	}

	public int getTemporaryHp() {
		return temporaryHp;
	}

	public int getTotalHp() {
		return totalHp;
	}

	public int getWound() {
		return wound;
	}

	public String getIdeals() {
		return ideals;
	}

	public int getInit() {
		return init;
	}

	public int getInspiration() {
		return inspiration;
	}

	public FgInventoryItem[] getInventory() {
		return inventory;
	}

	public String[] getLanguages() {
		return languages;
	}

	public int getTotalLevel() {
		return totalLevel;
	}

	public String getNotes() {
		return notes;
	}

	public int getPassivePerc() {
		return passivePerc;
	}

	public String getPersonalityTraits() {
		return personalityTraits;
	}

	public HashMap<String, FgClassPowers> getPowerGroups() {
		return powerGroups;
	}

	public int[] getPactSlots() {
		return pactSlots;
	}

	public int[] getSpellSlots() {
		return spellSlots;
	}

	public int getProfBonus() {
		return profBonus;
	}

	public String[] getProficiencies() {
		return proficiencies;
	}

	public String getRace() {
		return race;
	}

	public String getSenses() {
		return senses;
	}

	public HashMap<String, int[]> getSkills() {
		return skills;
	}

	public int getSpeed() {
		return speed;
	}

	public String getSpecialSpeed() {
		return specialSpeed;
	}

	public String[][] getTraits() {
		return traits;
	}

	public FgWeapon[] getWeapons() {
		return weapons;
	}

	public String getWeight() {
		return weight;
	}

}
