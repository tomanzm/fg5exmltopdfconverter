package converter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.io.File;
import java.io.IOException;

import org.apache.commons.text.WordUtils;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotationWidget;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.apache.pdfbox.pdmodel.interactive.form.PDTextField;

public class Converter implements Runnable {
	private PDDocument pdf = null;
	private FgXML characterData;
	private String extraInfo;
	private PDFMergerUtility ut;
	private FgClassPowers focusGroup;
	private FgClass focusClass;
	private String pdfStyle;
	private String xmlPath;
	private String threadName;
	private boolean swapScores;

	public enum PdfType {
		NORMAL, ALT, TYRANNY, LOG, DETAILS, SPELLS
	}

	@Override
	public void run() {
		File file = new File(xmlPath);
		File saved = new File(Converter.getFileNameWithoutExtension(file) + ".pdf");

		threadName = "Thread " + Thread.currentThread().getId() + ": ";

		System.out.println(threadName + "starting conversion");
		ArrayList<PdfType> pdfType = new ArrayList<PdfType>();
		switch (pdfStyle) {
		case "Default":
			pdfType.addAll(Arrays.asList(PdfType.NORMAL, PdfType.DETAILS));
			break;
		case "Alternative":
			pdfType.addAll(Arrays.asList(PdfType.ALT, PdfType.DETAILS));
			break;
		case "Tyranny of Dragons":
			pdfType.add(PdfType.TYRANNY);
			break;
		}
		pdfType.addAll(Arrays.asList(PdfType.SPELLS));
		try {
			this.characterData = new FgXML(xmlPath);
		} catch (Exception e) {
			System.out.println(threadName + "Error: " + e);
			e.printStackTrace(System.out);
			e.printStackTrace();
			return;
		}

		this.ut = new PDFMergerUtility();
		this.extraInfo = "";
		try {
			for (PdfType sheetType : pdfType) {
				switch (sheetType) {
				case NORMAL:
					pdf = PDDocument.load(new File("srcPdfs/Character Sheet - Form Fillable.pdf"));
					fillPage(pdf, sheetType);
					break;
				case ALT:
					pdf = PDDocument.load(new File("srcPdfs/Character Sheet - Alternative - Form Fillable.pdf"));
					fillPage(pdf, sheetType);
					break;
				case TYRANNY:
					pdf = PDDocument.load(new File("srcPdfs/CharacterSheet_TyrannyofDragons.pdf"));
					fillPage(pdf, sheetType);
					break;
				case LOG:
					pdf = PDDocument.load(new File("srcPdfs/DnDAdvLgLogsheet_Fillable.pdf"));
					break;
				case DETAILS:
					this.fillAndAppendPage("srcPdfs/Character Details (Optional) - Form Fillable.pdf", sheetType);
					break;
				case SPELLS:
					this.fillAndAppendPage("srcPdfs/Spellcasting Sheet (Optional) - Form Fillable.pdf", sheetType);
					break;
				}
			}

			String[] possibleOut = validateLines(extraInfo, 165, 270, false, "");
			for (int i = 0; i < possibleOut.length; ++i)
				this.addBlankPage(i, possibleOut[i]);

			this.getPdf().save(saved);
			System.out.println(threadName + "Conversion complete.\nFile is at: " + saved.getAbsolutePath());
		} catch (Exception e) {
			System.out.println(threadName + "Error: " + e);
			e.printStackTrace(System.out);
			e.printStackTrace();
		} 
	}

	public Converter(String pdfStyle, String xmlPath, boolean swapScores) {
		this.pdfStyle = pdfStyle;
		this.xmlPath = xmlPath;
		this.swapScores = swapScores;
	}

	public PDDocument getPdf() {
		return pdf;
	}

	public void setPdf(PDDocument pdf) {
		this.pdf = pdf;
	}

	public void fillAndAppendPage(String path, PdfType sheetType) throws IOException {
		switch (sheetType) {
		case SPELLS:
			// grab casters in array
			for (FgClass fgClass : characterData.getClasses()) {
				if (fgClass.isCaster()) {
					focusGroup = characterData.getPowerGroups().get(fgClass.getSpellGroupName());
					focusClass = fgClass;
					PDDocument nextDoc = PDDocument.load(new File(path));
					fillPage(nextDoc, sheetType);
					ut.appendDocument(pdf, nextDoc);
					nextDoc.close();
				}

			}
			// find caster power group
			// fill page with power group
			break;
		default:
			PDDocument nextDoc = PDDocument.load(new File(path));
			fillPage(nextDoc, sheetType);

			ut.appendDocument(pdf, nextDoc);
			nextDoc.close();
		}
	}

	public void printFields() throws IOException {
		PDDocumentCatalog docCatalog = pdf.getDocumentCatalog();
		PDAcroForm acroForm = docCatalog.getAcroForm();
		List<PDField> fields = acroForm.getFields();

		System.out.println(fields.size() + " top-level fields were found on the form");

		for (PDField field : fields) {
			processField(field);
		}
	}

	public PDDocument fillPage(PDDocument locPdf, PdfType pdfType) throws IOException {

		PDAcroForm acroForm = locPdf.getDocumentCatalog().getAcroForm();
		Iterator<PDField> it = acroForm.getFields().iterator();
		while (it.hasNext()) {
			PDField field = it.next();
			if (field instanceof PDTextField) {
				PDTextField textField = (PDTextField) field;
				textField.setDefaultAppearance("/Helv 0 Tf 0 g");
			}
		}
		acroForm.setNeedAppearances(true);
		switch (pdfType) {
		case NORMAL: {
			System.out.println(threadName + "Filling Default");
			acroForm.getField("CharacterName").setValue(characterData.getCharacterName());
			String temp = "";
			String hitDice = "";
			String totalHitDice = "";
			int o = 1;
			for (FgClass fgClass : characterData.getClasses()) {
				temp += fgClass.getName() + "(" + fgClass.getLevel() + ") ";
				hitDice += (o > 1 ? "/" : "") + fgClass.getHitDie();
				totalHitDice += (o > 1 ? "/" : "") + fgClass.getLevel();
				if (o % 4 == 0)
					temp += "\n";
				++o;

			}
			// Top Info
			acroForm.getField("ClassLevel").setValue(temp);
			acroForm.getField("Background").setValue(characterData.getBackground());
			acroForm.getField("Race ").setValue(characterData.getRace());
			acroForm.getField("Alignment").setValue(characterData.getAlignment());
			temp = characterData.getExp() + "/" + characterData.getExpNeeded();
			acroForm.getField("XP").setValue(temp);

			// Inspiration
			acroForm.getField("Inspiration").setValue("" + characterData.getInspiration());

			// Proficiency Bonus
			acroForm.getField("ProfBonus").setValue("+" + characterData.getProfBonus());

			// Ability/Save Values
			String[][] absSaveVals = { { "Strength", "STR", "STRmod", "ST Strength", "Check Box 11" },
					{ "Dexterity", "DEX", "DEXmod ", "ST Dexterity", "Check Box 18" },
					{ "Constitution", "CON", "CONmod", "ST Constitution", "Check Box 19" },
					{ "Intelligence", "INT", "INTmod", "ST Intelligence", "Check Box 20" },
					{ "Wisdom", "WIS", "WISmod", "ST Wisdom", "Check Box 21" },
					{ "Charisma", "CHA", "CHamod", "ST Charisma", "Check Box 22" } };
			for (String[] absSav : absSaveVals)
				fillAbility(acroForm, pdfType, absSav[0], absSav[1], absSav[2], absSav[3], absSav[4]);

			// Skills
			String[][] skillsVals = { { "Acrobatics", "Acrobatics", "Check Box 23" },
					{ "Animal Handling", "Animal", "Check Box 24" }, { "Arcana", "Arcana", "Check Box 25" },
					{ "Athletics", "Athletics", "Check Box 26" }, { "Deception", "Deception ", "Check Box 27" },
					{ "History", "History ", "Check Box 28" }, { "Insight", "Insight", "Check Box 29" },
					{ "Intimidation", "Intimidation", "Check Box 30" },
					{ "Investigation", "Investigation ", "Check Box 31" }, { "Medicine", "Medicine", "Check Box 32" },
					{ "Nature", "Nature", "Check Box 33" }, { "Perception", "Perception ", "Check Box 34" },
					{ "Performance", "Performance", "Check Box 35" }, { "Persuasion", "Persuasion", "Check Box 36" },
					{ "Religion", "Religion", "Check Box 37" }, { "Sleight of Hand", "SleightofHand", "Check Box 38" },
					{ "Stealth", "Stealth ", "Check Box 39" }, { "Survival", "Survival", "Check Box 40" }, };
			for (String[] skillVal : skillsVals)
				fillSkills(acroForm, pdfType, skillVal[0], skillVal[1], skillVal[2]);

			// Passive Perception
			acroForm.getField("Passive").setValue("" + characterData.getPassivePerc());

			// Proficiencies
			temp = "Armor/Weapons/Tools\n";
			if (characterData.getProficiencies() != null)
				for (String prof : characterData.getProficiencies())
					temp += prof + "\n";
			temp += "\nLanguages\n";
			if (characterData.getLanguages() != null)
				for (String lang : characterData.getLanguages())
					temp += lang + "\n";
			acroForm.getField("ProficienciesLang").setValue(temp);

			// AC
			acroForm.getField("AC").setValue("" + characterData.getAc());

			// Initiative
			acroForm.getField("Initiative").setValue(
					characterData.getInit() >= 0 ? "+" + characterData.getInit() : "" + characterData.getInit());

			// Speed
			acroForm.getField("Speed").setValue("" + characterData.getSpeed());

			// Personality Details
			acroForm.getField("PersonalityTraits ").setValue("" + characterData.getPersonalityTraits());
			acroForm.getField("Ideals").setValue("" + characterData.getIdeals());
			acroForm.getField("Bonds").setValue("" + characterData.getBonds());
			acroForm.getField("Flaws").setValue("" + characterData.getFlaws());

			// Features & Traits
			temp = "Features\n";
			extraInfo += "Features\n";
			if (characterData.getFeatures() != null)
				for (String origin : characterData.getFeatures().keySet()) {
					temp += "[" + origin + "]\n";
					extraInfo += "\n--" + origin + "--\n";
					for (String[] data : characterData.getFeatures().get(origin)) {
						temp += "-" + data[0] + "\n";
						extraInfo += "-" + data[0] + "-\n" + data[1];
					}
				}

			temp += "\nTraits\n";
			extraInfo += "\nTraits\n";
			if (characterData.getTraits() != null)
				for (int i = 0; i < characterData.getTraits().length; ++i) {
					temp += characterData.getTraits()[i][0] + "\n";
					extraInfo += "-" + characterData.getTraits()[i][0] + "-\n" + characterData.getTraits()[i][1] + "\n";
				}

			temp += "\nFeats\n";
			extraInfo += "\nFeats\n";
			if (characterData.getFeats() != null)
				for (Map.Entry<String, String> entry : characterData.getFeats().entrySet()) {
					temp += entry.getKey() + "\n";
					extraInfo += "-" + entry.getKey() + "-\n" + entry.getValue() + "\n";
				}
			acroForm.getField("Features and Traits").setValue(temp);
			// Health info
			acroForm.getField("HPMax").setValue("" + characterData.getTotalHp());
			acroForm.getField("HDTotal").setValue(totalHitDice);
			acroForm.getField("HD").setValue(hitDice);

			// Weapon Attacks
			temp = "";
			int wepCounter = 0;
			if (characterData.getWeapons() != null)
				for (FgWeapon wep : characterData.getWeapons()) {
					switch (wepCounter++) {
					case 0:
						fillWeapon(acroForm, wep, "Wpn Name", "Wpn1 AtkBonus", "Wpn1 Damage", false);
						break;
					case 1:
						fillWeapon(acroForm, wep, "Wpn Name 2", "Wpn2 AtkBonus ", "Wpn2 Damage ", false);
						break;
					case 2:
						fillWeapon(acroForm, wep, "Wpn Name 3", "Wpn3 AtkBonus  ", "Wpn3 Damage ", false);
						break;
					default:
						temp += fillWeapon(null, wep, "", "", "", true) + "\n";
					}
				}
			acroForm.getField("AttacksSpellcasting").setValue(temp);

			// Coinage
			temp = "";
			if (characterData.getCoinage() != null)
				for (Map.Entry<String, Integer> entry : characterData.getCoinage().entrySet()) {

					switch (entry.getKey().toLowerCase()) {
					case "cp":
						handleDefaultCoinage(acroForm, entry, "CP");
						break;
					case "sp":
						handleDefaultCoinage(acroForm, entry, "SP");
						break;
					case "ep":
						handleDefaultCoinage(acroForm, entry, "EP");
						break;
					case "gp":
						handleDefaultCoinage(acroForm, entry, "GP");
						break;
					case "pp":
						handleDefaultCoinage(acroForm, entry, "PP");
						break;
					default:
						temp += entry.getKey() + "=" + entry.getValue() + "\n";
					}
				}
			extraInfo += "\nInventory Items\n";
			// Equipment
			if (characterData.getInventory() != null)
				for (FgInventoryItem item : characterData.getInventory()) {
					String usedName = !item.isIdentified() && item.getNonIdName() != null ? item.getNonIdName()
							: item.getName();
					temp += usedName + (item.getCount() > 1 ? " x" + item.getCount() : "") + "\n";
					extraInfo += "\n[" + usedName + "]\n";

					switch (item.getCarriedStatus()) {
					case CARRIED:
						extraInfo += "Carried";
						break;
					case EQUIPPED:
						extraInfo += "Equipped";
						break;
					case NOTCARRIED:
					default:
						extraInfo += "Not Carried";
					}
					extraInfo += (item.getLocation() != null ? " - " + item.getLocation() : "") + "\n";
					// guaranteed: ac bonus carried count description weight
					if (item.isIdentified()) {
						extraInfo += "Type:" + item.getType()
								+ (item.getSubtype() != null ? "-" + item.getSubtype() : "")
								+ (item.getRarity() != null ? " <Rarity: " + item.getRarity() + ">" : "")
								+ (item.getCost() != null ? " <Cost: " + item.getCost() + ">" : "") + " <Weight: "
								+ item.getWeight() + "units>\n";
						if (!item.getDescription().trim().equals(""))
							extraInfo += item.getDescription().trim() + "\n";

					} else if (item.getNonIdDescription() != null && !item.getNonIdDescription().trim().equals(""))
						extraInfo += item.getNonIdDescription().trim() + "\n";
				}
			acroForm.getField("Equipment").setValue(validateLines(temp, 33, 500, true, ", ")[0]);
		}
			break;
		case ALT: {
			System.out.println(threadName + "Filling Alternative");
			acroForm.getField("CharacterName").setValue(characterData.getCharacterName());
			String temp = "";
			String hitDice = "";
			String totalHitDice = "";
			int o = 1;
			for (FgClass fgClass : characterData.getClasses()) {
				temp += fgClass.getName() + "(" + fgClass.getLevel() + ") ";
				hitDice += (o > 1 ? "/" : "") + fgClass.getHitDie();
				totalHitDice += (o > 1 ? "/" : "") + fgClass.getLevel();
				if (o % 4 == 0)
					temp += "\n";
				++o;

			}
			// Top Info
			acroForm.getField("ClassLevel").setValue(temp);
			acroForm.getField("Background").setValue(characterData.getBackground());
			acroForm.getField("Race ").setValue(characterData.getRace());
			acroForm.getField("Alignment").setValue(characterData.getAlignment());
			temp = characterData.getExp() + "/" + characterData.getExpNeeded();
			acroForm.getField("XP").setValue(temp);

			// Inspiration
			acroForm.getField("Inspiration").setValue("" + characterData.getInspiration());

			// Proficiency Bonus
			acroForm.getField("ProfBonus").setValue("+" + characterData.getProfBonus());

			// Ability/Save Values
			String[][] absSaveVals = { { "Strength", "STR", "STRmod", "SavingThrows", "ST Strength" },
					{ "Dexterity", "DEX", "DEXmod ", "SavingThrows2", "ST Dexterity" },
					{ "Constitution", "CON", "CONmod", "SavingThrows3", "ST Constitution" },
					{ "Intelligence", "INT", "INTmod", "SavingThrows4", "ST Intelligence" },
					{ "Wisdom", "WIS", "WISmod", "SavingThrows5", "ST Wisdom" },
					{ "Charisma", "CHA", "CHamod", "SavingThrows6", "ST Charisma" } };
			for (String[] absSav : absSaveVals)
				fillAbility(acroForm, pdfType, absSav[0], absSav[1], absSav[2], absSav[3], absSav[4]);

			// Skills
			String[][] skillsVals = { { "Acrobatics", "Acrobatics", "ChBx Acrobatics" },
					{ "Animal Handling", "Animal Handling", "ChBx Animal" }, { "Arcana", "Arcana", "ChBx Arcana" },
					{ "Athletics", "Athletics", "ChBx Athletics" }, { "Deception", "Deception", "ChBx Deception" },
					{ "History", "History", "ChBx History" }, { "Insight", "Insight", "ChBx Insight" },
					{ "Intimidation", "Intimidation", "ChBx Intimidation" },
					{ "Investigation", "Investigation", "ChBx Investigation" },
					{ "Medicine", "Medicine", "ChBx Medicine" }, { "Nature", "Nature", "ChBx Nature" },
					{ "Perception", "Perception", "ChBx Perception" },
					{ "Performance", "Performance", "ChBx Performance" },
					{ "Persuasion", "Persuasion", "ChBx Persuasion" }, { "Religion", "Religion", "ChBx Religion" },
					{ "Sleight of Hand", "SleightofHand", "ChBx Sleight" }, { "Stealth", "Stealth", "ChBx Stealth" },
					{ "Survival", "Survival", "ChBx Survival" }, };
			for (String[] skillVal : skillsVals)
				fillSkills(acroForm, pdfType, skillVal[0], skillVal[1], skillVal[2]);

			// Passive Perception
			acroForm.getField("Passive").setValue("" + characterData.getPassivePerc());

			// Proficiencies
			temp = "Armor/Weapons/Tools\n";
			if (characterData.getProficiencies() != null)
				for (String prof : characterData.getProficiencies())
					temp += prof + "\n";
			temp += "\nLanguages\n";
			if (characterData.getLanguages() != null)
				for (String lang : characterData.getLanguages())
					temp += lang + "\n";
			acroForm.getField("ProficienciesLang").setValue(temp);

			// AC
			acroForm.getField("AC").setValue("" + characterData.getAc());

			// Initiative
			acroForm.getField("Initiative").setValue(
					characterData.getInit() >= 0 ? "+" + characterData.getInit() : "" + characterData.getInit());

			// Speed
			acroForm.getField("Speed").setValue("" + characterData.getSpeed());

			// Personality Details
			acroForm.getField("PersonalityTraits ").setValue("" + characterData.getPersonalityTraits());
			acroForm.getField("Ideals").setValue("" + characterData.getIdeals());
			acroForm.getField("Bonds").setValue("" + characterData.getBonds());
			acroForm.getField("Flaws").setValue("" + characterData.getFlaws());

			// Features & Traits
			temp = "Features\n";
			extraInfo += "Features\n";
			if (characterData.getFeatures() != null)
				for (String origin : characterData.getFeatures().keySet()) {
					temp += "[" + origin + "]\n";
					extraInfo += "\n--" + origin + "--\n";
					for (String[] data : characterData.getFeatures().get(origin)) {
						temp += "-" + data[0] + "\n";
						extraInfo += "-" + data[0] + "-\n" + data[1];
					}
				}

			temp += "\nTraits\n";
			extraInfo += "\nTraits\n";
			if (characterData.getTraits() != null)
				for (int i = 0; i < characterData.getTraits().length; ++i) {
					temp += characterData.getTraits()[i][0] + "\n";
					extraInfo += "-" + characterData.getTraits()[i][0] + "-\n" + characterData.getTraits()[i][1] + "\n";
				}

			temp += "\nFeats\n";
			extraInfo += "\nFeats\n";
			if (characterData.getFeats() != null)
				for (Map.Entry<String, String> entry : characterData.getFeats().entrySet()) {
					temp += entry.getKey() + "\n";
					extraInfo += "-" + entry.getKey() + "-\n" + entry.getValue() + "\n";
				}
			acroForm.getField("Features and Traits").setValue(temp);
			// Health info
			acroForm.getField("HPMax").setValue("" + characterData.getTotalHp());
			acroForm.getField("HDTotal").setValue(totalHitDice);
			acroForm.getField("HD").setValue(hitDice);

			// Weapon Attacks
			temp = "";
			int wepCounter = 0;
			if (characterData.getWeapons() != null)
				for (FgWeapon wep : characterData.getWeapons()) {
					switch (wepCounter++) {
					case 0:
						fillWeapon(acroForm, wep, "Wpn Name", "Wpn1 AtkBonus", "Wpn1 Damage", false);
						break;
					case 1:
						fillWeapon(acroForm, wep, "Wpn Name 2", "Wpn2 AtkBonus ", "Wpn2 Damage ", false);
						break;
					case 2:
						fillWeapon(acroForm, wep, "Wpn Name 3", "Wpn3 AtkBonus  ", "Wpn3 Damage ", false);
						break;
					default:
						temp += fillWeapon(null, wep, "", "", "", true) + "\n";
					}
				}
			acroForm.getField("AttacksSpellcasting").setValue(temp);

			// Coinage
			temp = "Currency\n";
			if (characterData.getCoinage() != null)
				for (Map.Entry<String, Integer> entry : characterData.getCoinage().entrySet()) {
					temp += entry.getKey() + "=" + entry.getValue() + " ";
				}
			temp += "\nInventory Items\n";
			extraInfo += "\nInventory Items\n";
			// Equipment
			if (characterData.getInventory() != null)
				for (FgInventoryItem item : characterData.getInventory()) {
					String usedName = !item.isIdentified() && item.getNonIdName() != null ? item.getNonIdName()
							: item.getName();
					temp += usedName + (item.getCount() > 1 ? " x" + item.getCount() : "") + "\n";
					extraInfo += "\n[" + usedName + "]\n";

					switch (item.getCarriedStatus()) {
					case CARRIED:
						extraInfo += "Carried";
						break;
					case EQUIPPED:
						extraInfo += "Equipped";
						break;
					case NOTCARRIED:
					default:
						extraInfo += "Not Carried";
					}
					extraInfo += (item.getLocation() != null ? " - " + item.getLocation() : "") + "\n";
					// guaranteed: ac bonus carried count description weight
					if (item.isIdentified()) {
						extraInfo += "Type:" + item.getType()
								+ (item.getSubtype() != null ? "-" + item.getSubtype() : "")
								+ (item.getRarity() != null ? " <Rarity: " + item.getRarity() + ">" : "")
								+ (item.getCost() != null ? " <Cost: " + item.getCost() + ">" : "") + " <Weight: "
								+ item.getWeight() + "units>\n";
						if (!item.getDescription().trim().equals(""))
							extraInfo += item.getDescription().trim() + "\n";

					} else if (item.getNonIdDescription() != null && !item.getNonIdDescription().trim().equals(""))
						extraInfo += item.getNonIdDescription().trim() + "\n";
				}
			acroForm.getField("Equipment").setValue(validateLines(temp, 32, 500, true, ", ")[0]);
		}
			break;
		case TYRANNY: {
			System.out.println(threadName + "Filling Tyranny of Dragons");
			acroForm.getField("CharcterName").setValue(characterData.getCharacterName());
			String temp = "";
			String hitDice = "";
			String totalHitDice = "";
			int o = 1;
			for (FgClass fgClass : characterData.getClasses()) {
				temp += fgClass.getName() + "(" + fgClass.getLevel() + ") ";
				hitDice += (o > 1 ? "/" : "") + fgClass.getHitDie();
				totalHitDice += (o > 1 ? "/" : "") + fgClass.getLevel();
				if (o % 4 == 0)
					temp += "\n";
				++o;

			}
			// Top Info
			acroForm.getField("ClassLevel").setValue(temp);
			acroForm.getField("Background").setValue(characterData.getBackground());
			acroForm.getField("Race").setValue(characterData.getRace());
			acroForm.getField("Alignment").setValue(characterData.getAlignment());
			temp = characterData.getExp() + "/" + characterData.getExpNeeded();
			acroForm.getField("ExperiencePoints").setValue(temp);

			// Inspiration
			acroForm.getField("Inspiration").setValue("" + characterData.getInspiration());

			// Proficiency Bonus
			acroForm.getField("ProfBonus").setValue("+" + characterData.getProfBonus());

			// Ability/Save Values
			String[][] absSaveVals = { { "Strength", "Strength", "StrMod", "StrSave", "StrChk" },
					{ "Dexterity", "Dexterity", "DexMod", "DexSave", "DexChk" },
					{ "Constitution", "Constitution", "ConMod", "ConSave", "ConChk" },
					{ "Intelligence", "Intelligence", "IntMod", "IntSave", "IntChk" },
					{ "Wisdom", "Wisdom", "WisMod", "WisSave", "WisChk" },
					{ "Charisma", "Charisma", "ChaMod", "ChaSave", "ChaChk" } };
			for (String[] absSav : absSaveVals)
				fillAbility(acroForm, pdfType, absSav[0], absSav[1], absSav[2], absSav[3], absSav[4]);

			// Skills
			String[][] skillsVals = { { "Acrobatics", "Acrobatics", "AcroChk" },
					{ "Animal Handling", "AnimalHandling", "AnmHdlChk" }, { "Arcana", "Arcana", "ArcChk" },
					{ "Athletics", "Athletics", "AthChk" }, { "Deception", "Deception", "DecChk" },
					{ "History", "History", "HistChk" }, { "Insight", "Insight", "InsChk" },
					{ "Intimidation", "Intimidation", "IntimChk" }, { "Investigation", "Investigation", "InvesChk" },
					{ "Medicine", "Medicine", "MedChk" }, { "Nature", "Nature", "NatChk" },
					{ "Perception", "Perception", "PercChk" }, { "Performance", "Performance", "PerfChk" },
					{ "Persuasion", "Persuasion", "PersChk" }, { "Religion", "Religion", "RelChk" },
					{ "Sleight of Hand", "SlightOfHand", "SohChk" }, { "Stealth", "Stealth", "Stlth" },
					{ "Survival", "Survival", "SurvChk" }, };
			for (String[] skillVal : skillsVals)
				fillSkills(acroForm, pdfType, skillVal[0], skillVal[1], skillVal[2]);

			// Passive Perception
			acroForm.getField("PassivePerc").setValue("" + characterData.getPassivePerc());

			// Proficiencies
			temp = "Armor/Weapons/Tools\n";
			if (characterData.getProficiencies() != null)
				for (String prof : characterData.getProficiencies())
					temp += prof + "\n";
			temp += "\nLanguages\n";
			if (characterData.getLanguages() != null)
				for (String lang : characterData.getLanguages())
					temp += lang + "\n";
			acroForm.getField("ProficienciesLanguages").setValue(temp);

			// AC
			acroForm.getField("ArmorClass").setValue("" + characterData.getAc());

			// Initiative
			acroForm.getField("Initiative").setValue(
					characterData.getInit() >= 0 ? "+" + characterData.getInit() : "" + characterData.getInit());

			// Speed
			acroForm.getField("Speed").setValue("" + characterData.getSpeed());

			// Personality Details
			acroForm.getField("PersonalityTraits").setValue("" + characterData.getPersonalityTraits());
			acroForm.getField("Ideals").setValue("" + characterData.getIdeals());
			acroForm.getField("Bonds").setValue("" + characterData.getBonds());
			acroForm.getField("Flaws").setValue("" + characterData.getFlaws());

			// Features & Traits
			temp = "Features\n";
			extraInfo += "Features\n";
			if (characterData.getFeatures() != null)
				for (String origin : characterData.getFeatures().keySet()) {
					temp += "[" + origin + "]\n";
					extraInfo += "\n--" + origin + "--\n";
					for (String[] data : characterData.getFeatures().get(origin)) {
						temp += "-" + data[0] + "\n";
						extraInfo += "-" + data[0] + "-\n" + data[1];
					}
				}

			temp += "\nTraits\n";
			extraInfo += "\nTraits\n";
			if (characterData.getTraits() != null)
				for (int i = 0; i < characterData.getTraits().length; ++i) {
					temp += characterData.getTraits()[i][0] + "\n";
					extraInfo += "-" + characterData.getTraits()[i][0] + "-\n" + characterData.getTraits()[i][1] + "\n";
				}

			temp += "\nFeats\n";
			extraInfo += "\nFeats\n";
			if (characterData.getFeats() != null)
				for (Map.Entry<String, String> entry : characterData.getFeats().entrySet()) {
					temp += entry.getKey() + "\n";
					extraInfo += "-" + entry.getKey() + "-\n" + entry.getValue() + "\n";
				}
			acroForm.getField("FeaturesTraits").setValue(temp);
			// Health info
			acroForm.getField("HPMax").setValue("" + characterData.getTotalHp());
			acroForm.getField("TotalHitDice").setValue(totalHitDice);
			acroForm.getField("ExpendedHitDice").setValue(hitDice);

			// Weapon Attacks
			temp = "";
			int wepCounter = 0;
			if (characterData.getWeapons() != null)
				for (FgWeapon wep : characterData.getWeapons()) {
					switch (wepCounter++) {
					case 0:
						fillWeapon(acroForm, wep, "Weapon0", "AtkBonus0", "DamageType0", false);
						break;
					case 1:
						fillWeapon(acroForm, wep, "Weapon1", "AtkBonus1", "DamageType1", false);
						break;
					case 2:
						fillWeapon(acroForm, wep, "Weapon2", "AtkBonus2", "DamageType2", false);
						break;
					default:
						temp += fillWeapon(null, wep, "", "", "", true) + "\n";
					}
				}
			acroForm.getField("AttacksSpellcasting").setValue(temp);

			// Coinage
			temp = "";
			if (characterData.getCoinage() != null)
				for (Map.Entry<String, Integer> entry : characterData.getCoinage().entrySet()) {

					switch (entry.getKey().toLowerCase()) {
					case "cp":
						handleDefaultCoinage(acroForm, entry, "CP");
						break;
					case "sp":
						handleDefaultCoinage(acroForm, entry, "SP");
						break;
					case "ep":
						handleDefaultCoinage(acroForm, entry, "EP");
						break;
					case "gp":
						handleDefaultCoinage(acroForm, entry, "GP");
						break;
					case "pp":
						handleDefaultCoinage(acroForm, entry, "PP");
						break;
					default:
						temp += entry.getKey() + "=" + entry.getValue() + "\n";
					}
				}
			extraInfo += "\nInventory Items\n";
			// Equipment
			if (characterData.getInventory() != null)
				for (FgInventoryItem item : characterData.getInventory()) {
					String usedName = !item.isIdentified() && item.getNonIdName() != null ? item.getNonIdName()
							: item.getName();
					temp += usedName + (item.getCount() > 1 ? " x" + item.getCount() : "") + "\n";
					extraInfo += "\n[" + usedName + "]\n";

					switch (item.getCarriedStatus()) {
					case CARRIED:
						extraInfo += "Carried";
						break;
					case EQUIPPED:
						extraInfo += "Equipped";
						break;
					case NOTCARRIED:
					default:
						extraInfo += "Not Carried";
					}
					extraInfo += (item.getLocation() != null ? " - " + item.getLocation() : "") + "\n";
					// guaranteed: ac bonus carried count description weight
					if (item.isIdentified()) {
						extraInfo += "Type:" + item.getType()
								+ (item.getSubtype() != null ? "-" + item.getSubtype() : "")
								+ (item.getRarity() != null ? " <Rarity: " + item.getRarity() + ">" : "")
								+ (item.getCost() != null ? " <Cost: " + item.getCost() + ">" : "") + " <Weight: "
								+ item.getWeight() + "units>\n";
						if (!item.getDescription().trim().equals(""))
							extraInfo += item.getDescription().trim() + "\n";

					} else if (item.getNonIdDescription() != null && !item.getNonIdDescription().trim().equals(""))
						extraInfo += item.getNonIdDescription().trim() + "\n";
				}
			acroForm.getField("Equipment").setValue(validateLines(temp, 33, 500, true, ", ")[0]);

			System.out.println(threadName + "Filling Tyranny Character Details");
			// Top Info
			acroForm.getField("Age").setValue(characterData.getAge());
			acroForm.getField("Height").setValue(characterData.getHeight());
			acroForm.getField("Weight").setValue(characterData.getWeight());
			if (characterData.getAppearance() != null && !characterData.getAppearance().isEmpty())
				extraInfo += "\nAppearance\n" + characterData.getAppearance();

			// Backstory/Notes
			acroForm.getField("Backstory").setValue(characterData.getNotes());

			// Faction Name
			acroForm.getField("AllsOrgsR").setValue(characterData.getFaction());
		}
			break;
		case LOG:
			System.out.println(threadName + "Adventure Log");
			break;
		case DETAILS:
			System.out.println(threadName + "Filling Character Details");
			// Top Info
			acroForm.getField("CharacterName 2").setValue(characterData.getCharacterName());
			acroForm.getField("Age").setValue(characterData.getAge());
			acroForm.getField("Height").setValue(characterData.getHeight());
			acroForm.getField("Weight").setValue(characterData.getWeight());
			if (characterData.getAppearance() != null && !characterData.getAppearance().isEmpty())
				extraInfo += "\nAppearance\n" + characterData.getAppearance();

			// Backstory/Notes
			acroForm.getField("Backstory").setValue(characterData.getNotes());

			// Faction Name
			acroForm.getField("FactionName").setValue(characterData.getFaction());
			break;
		case SPELLS:
			System.out.println(threadName + "Filling spells for " + focusClass.getName());
			// fill out the top bar
			acroForm.getField("Spellcasting Class 2").setValue(focusClass.getName());
			String ability = focusClass.getSpellStat().substring(0, 1).toUpperCase()
					+ focusClass.getSpellStat().substring(1);
			int bonus = characterData.getProfBonus() + characterData.getAbilities().get(ability).getBonus();
			acroForm.getField("SpellcastingAbility 2").setValue(ability);
			acroForm.getField("SpellSaveDC  2").setValue("" + (8 + bonus));
			acroForm.getField("SpellAtkBonus 2").setValue(bonus >= 0 ? "+" + bonus : "" + bonus);
			if (focusClass.isPactCaster()) {
				int slot = 1;
				String value = "";
				boolean found = false;
				for (int data : characterData.getPactSlots()) {
					if (data > 0) {
						value = "" + data;
						found = true;
					} else
						value = "Pact";
					switch (slot++) {
					case 1:
						acroForm.getField("SlotsTotal 19").setValue(value);
						acroForm.getField("SlotsRemaining 19").setValue(found ? "0" : value);
						break;
					case 2:
						acroForm.getField("SlotsTotal 20").setValue(value);
						acroForm.getField("SlotsRemaining 20").setValue(found ? "0" : value);
						break;
					case 3:
						acroForm.getField("SlotsTotal 21").setValue(value);
						acroForm.getField("SlotsRemaining 21").setValue(found ? "0" : value);
						break;
					case 4:
						acroForm.getField("SlotsTotal 22").setValue(value);
						acroForm.getField("SlotsRemaining 22").setValue(found ? "0" : value);
						break;
					case 5:
						acroForm.getField("SlotsTotal 23").setValue(value);
						acroForm.getField("SlotsRemaining 23").setValue(found ? "0" : value);
						break;
					}
					if (found)
						break;
				}
			} else {
				int slot = 1;
				for (int data : characterData.getSpellSlots()) {
					switch (slot++) {
					case 1:
						acroForm.getField("SlotsTotal 19").setValue("" + data);
						acroForm.getField("SlotsRemaining 19").setValue("0");
						break;
					case 2:
						acroForm.getField("SlotsTotal 20").setValue("" + data);
						acroForm.getField("SlotsRemaining 20").setValue("0");
						break;
					case 3:
						acroForm.getField("SlotsTotal 21").setValue("" + data);
						acroForm.getField("SlotsRemaining 21").setValue("0");
						break;
					case 4:
						acroForm.getField("SlotsTotal 22").setValue("" + data);
						acroForm.getField("SlotsRemaining 22").setValue("0");
						break;
					case 5:
						acroForm.getField("SlotsTotal 23").setValue("" + data);
						acroForm.getField("SlotsRemaining 23").setValue("0");
						break;
					case 6:
						acroForm.getField("SlotsTotal 24").setValue("" + data);
						acroForm.getField("SlotsRemaining 24").setValue("0");
						break;
					case 7:
						acroForm.getField("SlotsTotal 25").setValue("" + data);
						acroForm.getField("SlotsRemaining 25").setValue("0");
						break;
					case 8:
						acroForm.getField("SlotsTotal 26").setValue("" + data);
						acroForm.getField("SlotsRemaining 26").setValue("0");
						break;
					case 9:
						acroForm.getField("SlotsTotal 27").setValue("" + data);
						acroForm.getField("SlotsRemaining 27").setValue("0");
						break;
					}

				}
			}

			// fill out the spells sections
			// all spell fields and their limits
			String[] spellFields = { "Spells 1014", "Spells 1016", "Spells 1017", "Spells 1018", "Spells 1019",
					"Spells 1020", "Spells 1021", "Spells 1022", "Spells 1015", "Spells 1023", "Spells 1024",
					"Spells 1025", "Spells 1026", "Spells 1027", "Spells 1028", "Spells 1029", "Spells 1030",
					"Spells 1031", "Spells 1032", "Spells 1033", "Spells 1046", "Spells 1034", "Spells 1035",
					"Spells 1036", "Spells 1037", "Spells 1038", "Spells 1039", "Spells 1040", "Spells 1041",
					"Spells 1042", "Spells 1043", "Spells 1044", "Spells 1045", "Spells 1048", "Spells 1047",
					"Spells 1049", "Spells 1050", "Spells 1051", "Spells 1052", "Spells 1053", "Spells 1054",
					"Spells 1055", "Spells 1056", "Spells 1057", "Spells 1058", "Spells 1059", "Spells 1061",
					"Spells 1060", "Spells 1062", "Spells 1063", "Spells 1064", "Spells 1065", "Spells 1066",
					"Spells 1067", "Spells 1068", "Spells 1069", "Spells 1070", "Spells 1071", "Spells 1072",
					"Spells 1074", "Spells 1073", "Spells 1075", "Spells 1076", "Spells 1077", "Spells 1078",
					"Spells 1079", "Spells 1080", "Spells 1081", "Spells 1083", "Spells 1082", "Spells 1084",
					"Spells 1085", "Spells 1086", "Spells 1087", "Spells 1088", "Spells 1089", "Spells 1090",
					"Spells 1092", "Spells 1091", "Spells 1093", "Spells 1094", "Spells 1095", "Spells 1096",
					"Spells 1097", "Spells 1098", "Spells 1099", "Spells 10101", "Spells 10100", "Spells 10102",
					"Spells 10103", "Spells 10104", "Spells 10105", "Spells 10106", "Spells 10108", "Spells 10107",
					"Spells 10109", "Spells 101010", "Spells 101011", "Spells 101012", "Spells 101013" };
			String[] preppedFields = { "Check Box 251", "Check Box 309", "Check Box 3010", "Check Box 3011",
					"Check Box 3012", "Check Box 3013", "Check Box 3014", "Check Box 3015", "Check Box 3016",
					"Check Box 3017", "Check Box 3018", "Check Box 3019", "Check Box 313", "Check Box 310",
					"Check Box 3020", "Check Box 3021", "Check Box 3022", "Check Box 3023", "Check Box 3024",
					"Check Box 3025", "Check Box 3026", "Check Box 3027", "Check Box 3028", "Check Box 3029",
					"Check Box 3030", "Check Box 315", "Check Box 314", "Check Box 3031", "Check Box 3032",
					"Check Box 3033", "Check Box 3034", "Check Box 3035", "Check Box 3036", "Check Box 3037",
					"Check Box 3038", "Check Box 3039", "Check Box 3040", "Check Box 3041", "Check Box 317",
					"Check Box 316", "Check Box 3042", "Check Box 3043", "Check Box 3044", "Check Box 3045",
					"Check Box 3046", "Check Box 3047", "Check Box 3048", "Check Box 3049", "Check Box 3050",
					"Check Box 3051", "Check Box 3052", "Check Box 319", "Check Box 318", "Check Box 3053",
					"Check Box 3054", "Check Box 3055", "Check Box 3056", "Check Box 3057", "Check Box 3058",
					"Check Box 3059", "Check Box 321", "Check Box 320", "Check Box 3060", "Check Box 3061",
					"Check Box 3062", "Check Box 3063", "Check Box 3064", "Check Box 3065", "Check Box 3066",
					"Check Box 323", "Check Box 322", "Check Box 3067", "Check Box 3068", "Check Box 3069",
					"Check Box 3070", "Check Box 3071", "Check Box 3072", "Check Box 3073", "Check Box 325",
					"Check Box 324", "Check Box 3074", "Check Box 3075", "Check Box 3076", "Check Box 3077",
					"Check Box 3078", "Check Box 327", "Check Box 326", "Check Box 3079", "Check Box 3080",
					"Check Box 3081", "Check Box 3082", "Check Box 3083" };
			int[] spellfieldLengths = { 8, 12, 13, 13, 13, 9, 9, 9, 7, 7 };

			if (focusGroup != null) {
				ArrayList<ArrayList<FgPower>> allSpells = new ArrayList<ArrayList<FgPower>>(Arrays.asList(
						focusGroup.getCantrips(), focusGroup.getLevel1(), focusGroup.getLevel2(),
						focusGroup.getLevel3(), focusGroup.getLevel4(), focusGroup.getLevel5(), focusGroup.getLevel6(),
						focusGroup.getLevel7(), focusGroup.getLevel8(), focusGroup.getLevel9()));
				int start = 0;
				// Cantrips
				for (int i = start; i < allSpells.get(0).size() && i < spellfieldLengths[0]; ++i)
					acroForm.getField(spellFields[i]).setValue(focusGroup.getCantrips().get(i).getName());

				// rest of spells
				for (int i = 1; i < allSpells.size(); ++i) {
					start += spellfieldLengths[i - 1];
					fillSpellFields((ArrayList<FgPower>) allSpells.get(i), i, start, acroForm, spellfieldLengths,
							spellFields, preppedFields);
				}
			}
			break;

		}
		return locPdf;
	}

	private void fillSpellFields(ArrayList<FgPower> spellArray, int spellLevel, int start, PDAcroForm acroForm,
			int[] spellfieldLengths, String[] spellFields, String[] preppedFields) throws IOException {
		boolean preparedDone = false;
		for (int spellFCount = start, i = 0; i < spellArray.size()
				&& spellFCount - start < spellfieldLengths[spellLevel]; ++i, ++spellFCount) {
			if (spellArray.get(i).isPrepared() && !focusClass.isPactCaster() && !preparedDone) {
				acroForm.getField(spellFields[spellFCount]).setValue(spellArray.get(i).getName());
				acroForm.getField(preppedFields[spellFCount - spellfieldLengths[0]]).setValue("Yes");
			} else if (preparedDone && !spellArray.get(i).isPrepared() || focusClass.isPactCaster())
				acroForm.getField(spellFields[spellFCount]).setValue(spellArray.get(i).getName());
			else
				--spellFCount;

			if (!focusClass.isPactCaster() && i + 1 >= spellArray.size() && !preparedDone) {
				i = -1;
				preparedDone = true;
			}
		}
	}

	private String[] validateLines(String inString, int maxLines, int maxWidth, boolean potentialInline,
			String inlineSeparator) {
		ArrayList<String> out = new ArrayList<String>();
		Scanner data = new Scanner(inString);
		String inlineOutput = "";
		int count = 0;
		if (potentialInline) {
			while (data.hasNextLine()) {
				++count;
				inlineOutput += (count == 1 ? data.nextLine() : inlineSeparator + data.nextLine());
			}
			data.close();
			if (count > maxLines) {
				out.add(inlineOutput);
			} else {
				out.add(inString);
			}
		} else {
			Scanner data2 = null;
			boolean lastEmpty = false;
			while (data.hasNextLine()) {
				String nextLine = data.nextLine();
				if (nextLine.trim().isEmpty()) {
					if (!lastEmpty) {
						inlineOutput += "\n";
						++count;
						if (count == maxLines) {
							count = 0;
							out.add(inlineOutput);
							inlineOutput = "";
						}
					}
					lastEmpty = true;
				} else {
					data2 = new Scanner(WordUtils.wrap(nextLine, maxWidth, "\n", false));
					lastEmpty = false;
					while (data2.hasNextLine()) {
						inlineOutput += data2.nextLine() + "\n";
						++count;
						if (count == maxLines) {
							count = 0;
							out.add(inlineOutput);
							inlineOutput = "";
						}
					}
				}
			}
			if (count < maxLines)
				out.add(inlineOutput);
			data2.close();
			data.close();
		}
		String[] outArr = new String[out.size()];
		return out.toArray(outArr);
	}

	private void handleDefaultCoinage(PDAcroForm acroForm, Map.Entry<String, Integer> entry, String coin)
			throws IOException {
		PDField tField = acroForm.getField(coin);
		String fieldVal = tField.getValueAsString();
		if (fieldVal.trim().isEmpty())
			tField.setValue("" + entry.getValue());
		else {
			tField.setValue("" + (Integer.parseInt(fieldVal) + entry.getValue()));
		}
	}

	private String fillWeapon(PDAcroForm acroForm, FgWeapon wep, String name, String atkBonus, String wpnDamage,
			boolean isDefault) throws IOException {
		int tAtk = weaponAtkBonus(wep);
		if (!isDefault) {
			acroForm.getField(name).setValue(wep.getName());
			tAtk = weaponAtkBonus(wep);

			acroForm.getField(atkBonus).setValue(tAtk >= 0 ? "+" + tAtk : "" + tAtk);
			String temp = "";
			for (int i = 0; i < wep.getDamage().length; ++i) {
				if (i > 0)
					temp += "\n";
				temp += wep.getDamage()[i][1] + weaponDMGBonus(wep, wep.getDamage()[i]) + " " + wep.getDamage()[i][4];
			}
			acroForm.getField(wpnDamage).setValue(temp);
			return "";
		} else {
			String temp = wep.getName() + " " + (tAtk >= 0 ? "+" + tAtk : "" + tAtk) + " ";

			for (int i = 0; i < wep.getDamage().length; ++i) {
				temp += wep.getDamage()[i][1] + weaponDMGBonus(wep, wep.getDamage()[i]) + " " + wep.getDamage()[i][4]
						+ " ";
			}
			return temp;
		}
	}

	private int weaponAtkBonus(FgWeapon wep) {
		int out = wep.getAttackBonus();
		if (wep.isProficient())
			out += characterData.getProfBonus();
		String props = wep.getProperties().toLowerCase();
		int temp = 0;
		if (wep.getAttackStat().equalsIgnoreCase("base"))
			if (props.contains("finesse"))
				temp += (characterData.getAbilities().get("Strength").getBonus() > characterData.getAbilities()
						.get("Dexterity").getBonus() ? characterData.getAbilities().get("Strength").getBonus()
								: characterData.getAbilities().get("Dexterity").getBonus());
			else if (props.contains("ranged"))
				temp += characterData.getAbilities().get("Dexterity").getBonus();
			else
				temp += characterData.getAbilities().get("Strength").getBonus();
		else {
			String stat = wep.getAttackStat();
			temp += characterData.getAbilities().get(stat.substring(0, 1).toUpperCase() + stat.substring(1)).getBonus();
		}
		out += temp;
		return out;
	}

	private String weaponDMGBonus(FgWeapon wep, String[] dmg) {
		int out = Integer.parseInt(dmg[0]);
		String props = wep.getProperties().toLowerCase();
		int temp = 0;
		String stat = dmg[2];
		if (stat.equalsIgnoreCase("base"))
			if (props.contains("finesse"))
				temp += (characterData.getAbilities().get("Strength").getBonus() > characterData.getAbilities()
						.get("Dexterity").getBonus() ? characterData.getAbilities().get("Strength").getBonus()
								: characterData.getAbilities().get("Dexterity").getBonus())
						* Integer.parseInt(dmg[3]);
			else if (props.contains("ranged"))
				temp += characterData.getAbilities().get("Dexterity").getBonus() * Integer.parseInt(dmg[3]);
			else
				temp += characterData.getAbilities().get("Strength").getBonus() * Integer.parseInt(dmg[3]);
		else if (stat.equals("-")) {
			/* do nothing */
		} else if (stat.equals("prf")) {
			temp += characterData.getProfBonus() * Integer.parseInt(dmg[3]);
		} else if (stat.equals("level")) {
			temp += characterData.getTotalLevel() * Integer.parseInt(dmg[3]);
		} else {
			String[] matches = new String[] { "strength", "dexterity", "constitution", "intelligence", "wisdom",
					"charisma" };
			boolean isAbility = false;
			for (String s : matches)
				if (stat.equalsIgnoreCase(s)) {
					isAbility = true;
					break;
				}
			if (isAbility)
				temp += characterData.getAbilities().get(stat.substring(0, 1).toUpperCase() + stat.substring(1))
						.getBonus() * Integer.parseInt(dmg[3]);
			else {
				for (FgClass fgClass : characterData.getClasses()) {
					if (fgClass.getName().equalsIgnoreCase(stat)) {
						temp += fgClass.getLevel() * Integer.parseInt(dmg[3]);
						break;
					}
				}
			}
		}
		out += temp;
		return (out >= 0 ? "+" + out : "" + out);
	}

	private void fillAbility(PDAcroForm acroForm, PdfType pdfType, String ability, String field0, String field1,
			String field2, String field3) throws IOException {
		// System.out.println(ability);
		FgAbility tAbility = characterData.getAbilities().get(ability);

		acroForm.getField(swapScores ? field1 : field0).setValue("" + tAbility.getScore());
		acroForm.getField(swapScores ? field0 : field1)
				.setValue(tAbility.getBonus() >= 0 ? "+" + tAbility.getBonus() : "" + tAbility.getBonus());
		int val = (tAbility.isSaveProf() ? tAbility.getBonus() + characterData.getProfBonus() : tAbility.getBonus());
		acroForm.getField(field2).setValue(val >= 0 ? "+" + val : "" + val);
		acroForm.getField(field3).setValue(tAbility.isSaveProf() ? "Yes" : "Off");

	}

	private void fillSkills(PDAcroForm acroForm, PdfType pdfType, String skill, String field0, String field1)
			throws IOException {
		int[] tSkill = characterData.getSkills().get(skill);
		boolean isProf = tSkill[0] > 0;
		int val = (isProf ? tSkill[1] + characterData.getProfBonus() : tSkill[1]);
		acroForm.getField(field0).setValue(val >= 0 ? "+" + val : "" + val);
		acroForm.getField(field1).setValue(isProf ? "Yes" : "Off");

	}

	private void processField(PDField field) throws IOException {
		String partialName = field.getPartialName();
		String fieldValue = field.getValueAsString();
		String outputString = "";
		if (partialName != null) {
			outputString += "." + partialName;
		}
		outputString += " = " + fieldValue + ",  type=" + field.getClass().getSimpleName();
		System.out.println(outputString);
	}

	private void addBlankPage(int index, String inString) throws IOException {
		System.out.println(threadName + "Appending details page");
		PDDocument nDoc = new PDDocument();
		PDPage details = new PDPage();
		// pdf.addPage(details);
		PDFont font = PDType1Font.HELVETICA;
		PDResources resources = new PDResources();
		resources.put(COSName.getPDFName("Helv"), font);

		// Add a new AcroForm and add that to the document
		PDAcroForm acroForm = new PDAcroForm(pdf);
		nDoc.getDocumentCatalog().setAcroForm(acroForm);

		// Add and set the resources and default appearance at the form level
		acroForm.setDefaultResources(resources);

		// Acrobat sets the font size on the form level to be
		// auto sized as default. This is done by setting the font size to '0'
		String defaultAppearanceString = "/Helv 0 Tf 0 g";
		acroForm.setDefaultAppearance(defaultAppearanceString);

		// Add a form field to the form.
		PDTextField textBox = new PDTextField(acroForm);
		textBox.setPartialName("Overflow" + index);
		textBox.setMultiline(true);
		textBox.setDoNotScroll(true);

		// add the field to the acroform
		acroForm.getFields().add(textBox);

		// Specify the widget annotation associated with the field
		PDAnnotationWidget widget = textBox.getWidgets().get(0);
		PDRectangle rect = new PDRectangle(10, 10, details.getMediaBox().getWidth() - 20,
				details.getMediaBox().getHeight() - 20);
		widget.setRectangle(rect);
		widget.setPage(details);

		// make sure the widget annotation is visible on screen and paper
		widget.setPrinted(true);

		// Add the widget annotation to the page
		details.getAnnotations().add(widget);

		// set the field value
		textBox.setValue(new String(inString.getBytes("ISO-8859-1")));
		nDoc.addPage(details);
		ut.appendDocument(pdf, nDoc);
	}

	public static String getFileNameWithoutExtension(File file) {
		String fileName = "";

		try {
			if (file != null && file.exists()) {
				String name = file.getName();
				fileName = name.replaceFirst("[.][^.]+$", "");
			}
		} catch (Exception e) {
			e.printStackTrace();
			fileName = "";
		}

		return fileName;

	}

}
