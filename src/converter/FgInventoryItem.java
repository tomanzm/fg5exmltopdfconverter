package converter;

public class FgInventoryItem implements Comparable<FgInventoryItem> {

	private CarriedStatus carriedStatus;
	private String cost;
	private int count;
	private String damage;
	private String description;
	private boolean isIdentified;
	private String location;
	private String name;
	private String nonIdName;
	private String nonIdDescription;
	private String rarity;
	private String properties;
	private String subtype;
	private String type;
	private float weight;

	enum CarriedStatus {
		CARRIED, EQUIPPED, NOTCARRIED
	}

	public FgInventoryItem() {
	}

	public CarriedStatus getCarriedStatus() {
		return carriedStatus;
	}

	public void setCarriedStatus(int carriedStatus) {
		switch (carriedStatus) {

		case 1:
			this.carriedStatus = CarriedStatus.CARRIED;
			break;
		case 2:
			this.carriedStatus = CarriedStatus.EQUIPPED;
			break;
		case 0:
		default:
			this.carriedStatus = CarriedStatus.NOTCARRIED;
		}
	}

	public String getCost() {
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getDamage() {
		return damage;
	}

	public void setDamage(String damage) {
		this.damage = damage;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isIdentified() {
		return isIdentified;
	}

	public void setIdentified(boolean isIdentified) {
		this.isIdentified = isIdentified;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNonIdName() {
		return nonIdName;
	}

	public void setNonIdName(String nonIdName) {
		this.nonIdName = nonIdName;
	}

	public String getNonIdDescription() {
		return nonIdDescription;
	}

	public void setNonIdDescription(String nonIdDescription) {
		this.nonIdDescription = nonIdDescription;
	}

	public String getRarity() {
		return rarity;
	}

	public void setRarity(String rarity) {
		this.rarity = rarity;
	}

	public String getProperties() {
		return properties;
	}

	public void setProperties(String properties) {
		this.properties = properties;
	}

	public String getSubtype() {
		return subtype;
	}

	public void setSubtype(String subtype) {
		this.subtype = subtype;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public float getWeight() {
		return weight;
	}

	public void setWeight(float weight) {
		this.weight = weight;
	}

	@Override
	public int compareTo(FgInventoryItem in) {
		String local = (!isIdentified && nonIdName != null ? nonIdName : name);
		String other = (!in.isIdentified() && in.getNonIdName() != null ? in.getNonIdName() : in.getName());
		return local.compareTo(other);
	}
}
