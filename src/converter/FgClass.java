package converter;

public class FgClass {

	private String hitDie;
	private int level;
	private String name;
	private boolean caster;
	private boolean pactCaster;
	private String spellGroupName;
	private String spellStat;
	
	public FgClass() {}

	public String getHitDie() {
		return hitDie;
	}

	public void setHitDie(String hitDie) {
		this.hitDie = hitDie;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isCaster() {
		return caster;
	}

	public void setCaster(boolean caster) {
		this.caster = caster;
	}

	public boolean isPactCaster() {
		return pactCaster;
	}

	public void setPactCaster(boolean pactCaster) {
		this.pactCaster = pactCaster;
	}

	public String getSpellGroupName() {
		return spellGroupName;
	}

	public void setSpellGroupName(String spellGroupName) {
		this.spellGroupName = spellGroupName;
	}

	public String getSpellStat() {
		return spellStat;
	}

	public void setSpellStat(String spellStat) {
		this.spellStat = spellStat;
	}

	@Override
	public String toString() {
		return "FgClass [hitDie=" + hitDie + ", level=" + level + ", name=" + name + ", caster=" + caster
				+ ", pactCaster=" + pactCaster + "]";
	}
	
	
}
