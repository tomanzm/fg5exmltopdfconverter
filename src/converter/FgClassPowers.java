package converter;

import java.util.ArrayList;

public class FgClassPowers {
	private boolean isSpellGroup;
	private ArrayList<FgPower> cantrips;
	private ArrayList<FgPower> level1;
	private ArrayList<FgPower> level2;
	private ArrayList<FgPower> level3;
	private ArrayList<FgPower> level4;
	private ArrayList<FgPower> level5;
	private ArrayList<FgPower> level6;
	private ArrayList<FgPower> level7;
	private ArrayList<FgPower> level8;
	private ArrayList<FgPower> level9;

	public FgClassPowers(boolean isSpellGroup) {
		this.isSpellGroup = isSpellGroup;
		cantrips = new ArrayList<FgPower>();
		if (isSpellGroup) {
			level1 = new ArrayList<FgPower>();
			level2 = new ArrayList<FgPower>();
			level3 = new ArrayList<FgPower>();
			level4 = new ArrayList<FgPower>();
			level5 = new ArrayList<FgPower>();
			level6 = new ArrayList<FgPower>();
			level7 = new ArrayList<FgPower>();
			level8 = new ArrayList<FgPower>();
			level9 = new ArrayList<FgPower>();
		}
	}

	public boolean isSpellGroup() {
		return isSpellGroup;
	}

	public void setSpellGroup(boolean isSpellGroup) {
		this.isSpellGroup = isSpellGroup;
	}

	public ArrayList<FgPower> getCantrips() {
		return cantrips;
	}

	public void setCantrips(ArrayList<FgPower> cantrips) {
		this.cantrips = cantrips;
	}

	public ArrayList<FgPower> getLevel1() {
		return level1;
	}

	public void setLevel1(ArrayList<FgPower> level1) {
		this.level1 = level1;
	}

	public ArrayList<FgPower> getLevel2() {
		return level2;
	}

	public void setLevel2(ArrayList<FgPower> level2) {
		this.level2 = level2;
	}

	public ArrayList<FgPower> getLevel3() {
		return level3;
	}

	public void setLevel3(ArrayList<FgPower> level3) {
		this.level3 = level3;
	}

	public ArrayList<FgPower> getLevel4() {
		return level4;
	}

	public void setLevel4(ArrayList<FgPower> level4) {
		this.level4 = level4;
	}

	public ArrayList<FgPower> getLevel5() {
		return level5;
	}

	public void setLevel5(ArrayList<FgPower> level5) {
		this.level5 = level5;
	}

	public ArrayList<FgPower> getLevel6() {
		return level6;
	}

	public void setLevel6(ArrayList<FgPower> level6) {
		this.level6 = level6;
	}

	public ArrayList<FgPower> getLevel7() {
		return level7;
	}

	public void setLevel7(ArrayList<FgPower> level7) {
		this.level7 = level7;
	}

	public ArrayList<FgPower> getLevel8() {
		return level8;
	}

	public void setLevel8(ArrayList<FgPower> level8) {
		this.level8 = level8;
	}

	public ArrayList<FgPower> getLevel9() {
		return level9;
	}

	public void setLevel9(ArrayList<FgPower> level9) {
		this.level9 = level9;
	}

}
