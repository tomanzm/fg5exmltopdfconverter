package converter;

public class FgAbility {
	
	private int bonus;
	private int save;
	private boolean saveProf;
	private int score;
	
	public FgAbility() {
	}

	public int getBonus() {
		return bonus;
	}

	public void setBonus(int bonus) {
		this.bonus = bonus;
	}

	public int getSave() {
		return save;
	}

	public void setSave(int save) {
		this.save = save;
	}

	public boolean isSaveProf() {
		return saveProf;
	}

	public void setSaveProf(boolean saveProf) {
		this.saveProf = saveProf;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	@Override
	public String toString() {
		return "FgAbility [bonus=" + bonus + ", save=" + save + ", saveProf=" + saveProf + ", score=" + score + "]";
	}
	
	
}
