package converter;

public class FgAdventure {
	private String adventureName;
	private String sessionNum;
	private String advDate;
	private String gmName;
	private String startingXP;
	private String earnedXP;
	private String totalXP;
	private String startingGold;
	private String earnedGold;
	private String totalGold;
	private String startingDT;
	private String earnedDT;
	private String totalDT;
	private String startingRN;
	private String earnedRN;
	private String totalRN;
	private String advDtNotes;
	private String startingMgc;
	private String earnedMgc;
	private String totalMgc;
	private String startingT2Mgc;
	private String earnedT2Mgc;
	private String totalT2Mgc;
	private String startingT3Mgc;
	private String earnedT3Mgc;
	private String totaltT3Mgc;
	private String startingT4Mgc;
	private String earnedT4Mgc;
	private String totaltT4Mgc;
	private String mgcItmUnlocked;
	private String stryRwds;
	private String purchases;

	public FgAdventure() {
	}

	public String getAdventureName() {
		return adventureName;
	}

	public void setAdventureName(String adventureName) {
		this.adventureName = adventureName;
	}

	public String getSessionNum() {
		return sessionNum;
	}

	public void setSessionNum(String sessionNum) {
		this.sessionNum = sessionNum;
	}

	public String getAdvDate() {
		return advDate;
	}

	public void setAdvDate(String advDate) {
		this.advDate = advDate;
	}

	public String getGmName() {
		return gmName;
	}

	public void setGmName(String gmName) {
		this.gmName = gmName;
	}

	public String getStartingXP() {
		return startingXP;
	}

	public void setStartingXP(String startingXP) {
		this.startingXP = startingXP;
	}

	public String getEarnedXP() {
		return earnedXP;
	}

	public void setEarnedXP(String earnedXP) {
		this.earnedXP = earnedXP;
	}

	public String getTotalXP() {
		return totalXP;
	}

	public void setTotalXP(String totalXP) {
		this.totalXP = totalXP;
	}

	public String getStartingGold() {
		return startingGold;
	}

	public void setStartingGold(String startingGold) {
		this.startingGold = startingGold;
	}

	public String getEarnedGold() {
		return earnedGold;
	}

	public void setEarnedGold(String earnedGold) {
		this.earnedGold = earnedGold;
	}

	public String getTotalGold() {
		return totalGold;
	}

	public void setTotalGold(String totalGold) {
		this.totalGold = totalGold;
	}

	public String getStartingDT() {
		return startingDT;
	}

	public void setStartingDT(String startingDT) {
		this.startingDT = startingDT;
	}

	public String getEarnedDT() {
		return earnedDT;
	}

	public void setEarnedDT(String earnedDT) {
		this.earnedDT = earnedDT;
	}

	public String getTotalDT() {
		return totalDT;
	}

	public void setTotalDT(String totalDT) {
		this.totalDT = totalDT;
	}

	public String getStartingRN() {
		return startingRN;
	}

	public void setStartingRN(String startingRN) {
		this.startingRN = startingRN;
	}

	public String getEarnedRN() {
		return earnedRN;
	}

	public void setEarnedRN(String earnedRN) {
		this.earnedRN = earnedRN;
	}

	public String getTotalRN() {
		return totalRN;
	}

	public void setTotalRN(String totalRN) {
		this.totalRN = totalRN;
	}

	public String getAdvDtNotes() {
		return advDtNotes;
	}

	public void setAdvDtNotes(String advDtNotes) {
		this.advDtNotes = advDtNotes;
	}

	public String getStartingMgc() {
		return startingMgc;
	}

	public void setStartingMgc(String startingMgc) {
		this.startingMgc = startingMgc;
	}

	public String getEarnedMgc() {
		return earnedMgc;
	}

	public void setEarnedMgc(String earnedMgc) {
		this.earnedMgc = earnedMgc;
	}

	public String getTotalMgc() {
		return totalMgc;
	}

	public void setTotalMgc(String totalMgc) {
		this.totalMgc = totalMgc;
	}

	public String getStartingT2Mgc() {
		return startingT2Mgc;
	}

	public void setStartingT2Mgc(String startingT2Mgc) {
		this.startingT2Mgc = startingT2Mgc;
	}

	public String getEarnedT2Mgc() {
		return earnedT2Mgc;
	}

	public void setEarnedT2Mgc(String earnedT2Mgc) {
		this.earnedT2Mgc = earnedT2Mgc;
	}

	public String getTotalT2Mgc() {
		return totalT2Mgc;
	}

	public void setTotalT2Mgc(String totalT2Mgc) {
		this.totalT2Mgc = totalT2Mgc;
	}

	public String getStartingT3Mgc() {
		return startingT3Mgc;
	}

	public void setStartingT3Mgc(String startingT3Mgc) {
		this.startingT3Mgc = startingT3Mgc;
	}

	public String getEarnedT3Mgc() {
		return earnedT3Mgc;
	}

	public void setEarnedT3Mgc(String earnedT3Mgc) {
		this.earnedT3Mgc = earnedT3Mgc;
	}

	public String getTotaltT3Mgc() {
		return totaltT3Mgc;
	}

	public void setTotaltT3Mgc(String totaltT3Mgc) {
		this.totaltT3Mgc = totaltT3Mgc;
	}

	public String getStartingT4Mgc() {
		return startingT4Mgc;
	}

	public void setStartingT4Mgc(String startingT4Mgc) {
		this.startingT4Mgc = startingT4Mgc;
	}

	public String getEarnedT4Mgc() {
		return earnedT4Mgc;
	}

	public void setEarnedT4Mgc(String earnedT4Mgc) {
		this.earnedT4Mgc = earnedT4Mgc;
	}

	public String getTotaltT4Mgc() {
		return totaltT4Mgc;
	}

	public void setTotaltT4Mgc(String totaltT4Mgc) {
		this.totaltT4Mgc = totaltT4Mgc;
	}

	public String getMgcItmUnlocked() {
		return mgcItmUnlocked;
	}

	public void setMgcItmUnlocked(String mgcItmUnlocked) {
		this.mgcItmUnlocked = mgcItmUnlocked;
	}

	public String getStryRwds() {
		return stryRwds;
	}

	public void setStryRwds(String stryRwds) {
		this.stryRwds = stryRwds;
	}

	public String getPurchases() {
		return purchases;
	}

	public void setPurchases(String purchases) {
		this.purchases = purchases;
	}

}
