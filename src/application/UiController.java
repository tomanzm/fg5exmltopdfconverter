package application;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.concurrent.locks.ReentrantLock;

import converter.Converter;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.FileChooser;

public class UiController implements Initializable {
	@FXML
	private Button browseButton;
	@FXML
	private Button convertBtn;
	@FXML
	private TextField xmlFilePath;
	@FXML
	private TextArea outputLog;
	@FXML
	private ChoiceBox<String> sheetStyle;
	@FXML
	private CheckBox swapScores;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		sheetStyle.setItems(FXCollections.observableArrayList("Default", "Alternative", "Tyranny of Dragons"));
		sheetStyle.getSelectionModel().selectFirst();
		Console console = new Console(outputLog);
		PrintStream ps = new PrintStream(console, true);
		System.setOut(ps);
	}

	public void browseForXml(ActionEvent event) {
		FileChooser chooser = new FileChooser();
		chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("XML Files", "*.xml"));
		chooser.setTitle("Open XML File");
		File file = chooser.showOpenDialog(browseButton.getScene().getWindow());
		if (file != null) {
			xmlFilePath.setText(file.toString());
		}
	}

	public void convertToPdf(ActionEvent event) {
		// parse as much as possible from the xml
		String xmlPath = xmlFilePath.getText();

		Thread td = null;
		if (!xmlPath.trim().isEmpty()) {
			// make sure the output file doesn't already exist, and if it does, handle it
			File file = new File(xmlPath);
			File saved = new File(Converter.getFileNameWithoutExtension(file) + ".pdf");
			if (saved.exists()) {
				Alert alert = new Alert(AlertType.CONFIRMATION);
				alert.setTitle("File Exists");
				alert.setHeaderText("The output file (" + saved.getAbsolutePath()
						+ ") already exists,\nwould you like to overwrite it?");
				ButtonType buttonYes = new ButtonType("Yes");
				alert.getButtonTypes().setAll(buttonYes, new ButtonType("No"));
				Optional<ButtonType> result = alert.showAndWait();
				if (result.get() != buttonYes) {
					System.out.println("Aborting conversion, please rename your xml file and try again");
					return;
				}
			}
			// initialize the converter with the first page based on loaded properties
			// and add what can be added for the page
			Converter cnv = new Converter(sheetStyle.getSelectionModel().getSelectedItem(), xmlPath, swapScores.isSelected());
			td = new Thread(cnv);
			td.start();
		}

	}

	public static class Console extends OutputStream {

		private TextArea output;
		private ReentrantLock lock = new ReentrantLock();

		public Console(TextArea ta) {
			this.output = ta;
		}

		@Override
		public void write(int i) throws IOException {
			lock.lock();
			javafx.application.Platform.runLater(() -> output.appendText(String.valueOf((char) i)));
			lock.unlock();
		}
	}
}
